--
-- PostgreSQL database dump
--

-- Dumped from database version 10.14 (Ubuntu 10.14-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.14 (Ubuntu 10.14-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO ebot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO ebot;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO ebot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO ebot;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO ebot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO ebot;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO ebot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO ebot;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO ebot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO ebot;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO ebot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO ebot;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO ebot;

--
-- Name: main_chat; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_chat (
    id integer NOT NULL,
    user_type character varying(250) NOT NULL,
    chat_id integer,
    first_name character varying(250),
    last_name character varying(250),
    username character varying(250),
    phone character varying(250),
    is_active boolean NOT NULL,
    selected_test_format character varying(25),
    would_like_mandatory_tests boolean,
    step character varying(255),
    balance double precision NOT NULL,
    page integer NOT NULL,
    payment_amount integer NOT NULL,
    user_id integer
);


ALTER TABLE public.main_chat OWNER TO ebot;

--
-- Name: main_chat_groups; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_chat_groups (
    id integer NOT NULL,
    chat_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.main_chat_groups OWNER TO ebot;

--
-- Name: main_chat_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_chat_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_chat_groups_id_seq OWNER TO ebot;

--
-- Name: main_chat_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_chat_groups_id_seq OWNED BY public.main_chat_groups.id;


--
-- Name: main_chat_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_chat_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_chat_id_seq OWNER TO ebot;

--
-- Name: main_chat_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_chat_id_seq OWNED BY public.main_chat.id;


--
-- Name: main_chat_selected_classes; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_chat_selected_classes (
    id integer NOT NULL,
    chat_id integer NOT NULL,
    class_id integer NOT NULL
);


ALTER TABLE public.main_chat_selected_classes OWNER TO ebot;

--
-- Name: main_chat_selected_classes_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_chat_selected_classes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_chat_selected_classes_id_seq OWNER TO ebot;

--
-- Name: main_chat_selected_classes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_chat_selected_classes_id_seq OWNED BY public.main_chat_selected_classes.id;


--
-- Name: main_chat_selected_subjects; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_chat_selected_subjects (
    id integer NOT NULL,
    chat_id integer NOT NULL,
    subject_id integer NOT NULL
);


ALTER TABLE public.main_chat_selected_subjects OWNER TO ebot;

--
-- Name: main_chat_selected_subjects_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_chat_selected_subjects_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_chat_selected_subjects_id_seq OWNER TO ebot;

--
-- Name: main_chat_selected_subjects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_chat_selected_subjects_id_seq OWNED BY public.main_chat_selected_subjects.id;


--
-- Name: main_chat_selected_topics; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_chat_selected_topics (
    id integer NOT NULL,
    chat_id integer NOT NULL,
    topic_id integer NOT NULL
);


ALTER TABLE public.main_chat_selected_topics OWNER TO ebot;

--
-- Name: main_chat_selected_topics_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_chat_selected_topics_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_chat_selected_topics_id_seq OWNER TO ebot;

--
-- Name: main_chat_selected_topics_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_chat_selected_topics_id_seq OWNED BY public.main_chat_selected_topics.id;


--
-- Name: main_class; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_class (
    id integer NOT NULL,
    title character varying(250) NOT NULL
);


ALTER TABLE public.main_class OWNER TO ebot;

--
-- Name: main_class_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_class_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_class_id_seq OWNER TO ebot;

--
-- Name: main_class_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_class_id_seq OWNED BY public.main_class.id;


--
-- Name: main_customuser; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_customuser (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(150) NOT NULL,
    last_name character varying(150) NOT NULL,
    phone character varying(60),
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    type_of_user character varying(150) NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    balance double precision NOT NULL
);


ALTER TABLE public.main_customuser OWNER TO ebot;

--
-- Name: main_customuser_groups; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_customuser_groups (
    id integer NOT NULL,
    customuser_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.main_customuser_groups OWNER TO ebot;

--
-- Name: main_customuser_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_customuser_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_customuser_groups_id_seq OWNER TO ebot;

--
-- Name: main_customuser_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_customuser_groups_id_seq OWNED BY public.main_customuser_groups.id;


--
-- Name: main_customuser_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_customuser_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_customuser_id_seq OWNER TO ebot;

--
-- Name: main_customuser_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_customuser_id_seq OWNED BY public.main_customuser.id;


--
-- Name: main_customuser_user_permissions; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_customuser_user_permissions (
    id integer NOT NULL,
    customuser_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.main_customuser_user_permissions OWNER TO ebot;

--
-- Name: main_customuser_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_customuser_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_customuser_user_permissions_id_seq OWNER TO ebot;

--
-- Name: main_customuser_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_customuser_user_permissions_id_seq OWNED BY public.main_customuser_user_permissions.id;


--
-- Name: main_group; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_group (
    id integer NOT NULL,
    title character varying(25) NOT NULL,
    statistic character varying(100)
);


ALTER TABLE public.main_group OWNER TO ebot;

--
-- Name: main_group_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_group_id_seq OWNER TO ebot;

--
-- Name: main_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_group_id_seq OWNED BY public.main_group.id;


--
-- Name: main_group_students; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_group_students (
    id integer NOT NULL,
    group_id integer NOT NULL,
    chat_id integer NOT NULL
);


ALTER TABLE public.main_group_students OWNER TO ebot;

--
-- Name: main_group_students_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_group_students_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_group_students_id_seq OWNER TO ebot;

--
-- Name: main_group_students_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_group_students_id_seq OWNED BY public.main_group_students.id;


--
-- Name: main_order; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_order (
    created_at timestamp with time zone,
    id integer NOT NULL,
    total_price numeric(9,2) NOT NULL,
    file character varying(100),
    is_paid boolean NOT NULL,
    test_type character varying(255),
    purchaser_id integer
);


ALTER TABLE public.main_order OWNER TO ebot;

--
-- Name: main_order_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_order_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_order_id_seq OWNER TO ebot;

--
-- Name: main_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_order_id_seq OWNED BY public.main_order.id;


--
-- Name: main_orderitem; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_orderitem (
    id integer NOT NULL,
    is_solved_correctly boolean NOT NULL,
    price numeric(9,2) NOT NULL,
    order_id integer NOT NULL,
    question_id integer NOT NULL
);


ALTER TABLE public.main_orderitem OWNER TO ebot;

--
-- Name: main_orderitem_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_orderitem_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_orderitem_id_seq OWNER TO ebot;

--
-- Name: main_orderitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_orderitem_id_seq OWNED BY public.main_orderitem.id;


--
-- Name: main_question; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_question (
    id integer NOT NULL,
    title character varying(250) NOT NULL,
    description text,
    is_mandatory boolean NOT NULL,
    is_state_center_test boolean NOT NULL,
    level integer,
    is_owner boolean NOT NULL,
    _class_id integer,
    owner_id integer,
    subject_id integer,
    topic_id integer,
    CONSTRAINT main_question_level_check CHECK ((level >= 0))
);


ALTER TABLE public.main_question OWNER TO ebot;

--
-- Name: main_question_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_question_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_question_id_seq OWNER TO ebot;

--
-- Name: main_question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_question_id_seq OWNED BY public.main_question.id;


--
-- Name: main_section; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_section (
    id integer NOT NULL,
    title character varying(250) NOT NULL,
    subject_id integer
);


ALTER TABLE public.main_section OWNER TO ebot;

--
-- Name: main_section_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_section_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_section_id_seq OWNER TO ebot;

--
-- Name: main_section_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_section_id_seq OWNED BY public.main_section.id;


--
-- Name: main_subject; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_subject (
    id integer NOT NULL,
    title character varying(250) NOT NULL,
    is_thematic boolean NOT NULL
);


ALTER TABLE public.main_subject OWNER TO ebot;

--
-- Name: main_subject_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_subject_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_subject_id_seq OWNER TO ebot;

--
-- Name: main_subject_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_subject_id_seq OWNED BY public.main_subject.id;


--
-- Name: main_topic; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_topic (
    id integer NOT NULL,
    title character varying(250) NOT NULL,
    _class_id integer,
    section_id integer,
    subject_id integer
);


ALTER TABLE public.main_topic OWNER TO ebot;

--
-- Name: main_topic_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_topic_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_topic_id_seq OWNER TO ebot;

--
-- Name: main_topic_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_topic_id_seq OWNED BY public.main_topic.id;


--
-- Name: main_transaction; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_transaction (
    id integer NOT NULL,
    amount double precision NOT NULL,
    created_at timestamp with time zone NOT NULL,
    payment_method character varying(255) NOT NULL,
    is_paid boolean NOT NULL,
    owner_id integer NOT NULL
);


ALTER TABLE public.main_transaction OWNER TO ebot;

--
-- Name: main_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_transaction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_transaction_id_seq OWNER TO ebot;

--
-- Name: main_transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_transaction_id_seq OWNED BY public.main_transaction.id;


--
-- Name: main_variant; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_variant (
    id integer NOT NULL,
    title character varying(250) NOT NULL,
    description text,
    is_correct boolean NOT NULL,
    question_id integer
);


ALTER TABLE public.main_variant OWNER TO ebot;

--
-- Name: main_variant_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_variant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_variant_id_seq OWNER TO ebot;

--
-- Name: main_variant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_variant_id_seq OWNED BY public.main_variant.id;


--
-- Name: main_withdraw; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.main_withdraw (
    id integer NOT NULL,
    status boolean NOT NULL,
    summa double precision NOT NULL,
    created_at timestamp with time zone,
    user_id integer
);


ALTER TABLE public.main_withdraw OWNER TO ebot;

--
-- Name: main_withdraw_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.main_withdraw_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.main_withdraw_id_seq OWNER TO ebot;

--
-- Name: main_withdraw_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.main_withdraw_id_seq OWNED BY public.main_withdraw.id;


--
-- Name: payment_paycomtransaction; Type: TABLE; Schema: public; Owner: ebot
--

CREATE TABLE public.payment_paycomtransaction (
    id integer NOT NULL,
    paycom_transaction_id character varying(100),
    paycom_time bigint,
    paycom_time_datetime timestamp with time zone,
    create_time timestamp with time zone NOT NULL,
    amount double precision NOT NULL,
    state integer NOT NULL,
    cancel_time timestamp with time zone,
    perform_time timestamp with time zone,
    reason integer,
    order_id integer NOT NULL
);


ALTER TABLE public.payment_paycomtransaction OWNER TO ebot;

--
-- Name: payment_paycomtransaction_id_seq; Type: SEQUENCE; Schema: public; Owner: ebot
--

CREATE SEQUENCE public.payment_paycomtransaction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.payment_paycomtransaction_id_seq OWNER TO ebot;

--
-- Name: payment_paycomtransaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: ebot
--

ALTER SEQUENCE public.payment_paycomtransaction_id_seq OWNED BY public.payment_paycomtransaction.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: main_chat id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat ALTER COLUMN id SET DEFAULT nextval('public.main_chat_id_seq'::regclass);


--
-- Name: main_chat_groups id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_groups ALTER COLUMN id SET DEFAULT nextval('public.main_chat_groups_id_seq'::regclass);


--
-- Name: main_chat_selected_classes id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_classes ALTER COLUMN id SET DEFAULT nextval('public.main_chat_selected_classes_id_seq'::regclass);


--
-- Name: main_chat_selected_subjects id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_subjects ALTER COLUMN id SET DEFAULT nextval('public.main_chat_selected_subjects_id_seq'::regclass);


--
-- Name: main_chat_selected_topics id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_topics ALTER COLUMN id SET DEFAULT nextval('public.main_chat_selected_topics_id_seq'::regclass);


--
-- Name: main_class id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_class ALTER COLUMN id SET DEFAULT nextval('public.main_class_id_seq'::regclass);


--
-- Name: main_customuser id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser ALTER COLUMN id SET DEFAULT nextval('public.main_customuser_id_seq'::regclass);


--
-- Name: main_customuser_groups id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser_groups ALTER COLUMN id SET DEFAULT nextval('public.main_customuser_groups_id_seq'::regclass);


--
-- Name: main_customuser_user_permissions id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.main_customuser_user_permissions_id_seq'::regclass);


--
-- Name: main_group id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_group ALTER COLUMN id SET DEFAULT nextval('public.main_group_id_seq'::regclass);


--
-- Name: main_group_students id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_group_students ALTER COLUMN id SET DEFAULT nextval('public.main_group_students_id_seq'::regclass);


--
-- Name: main_order id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_order ALTER COLUMN id SET DEFAULT nextval('public.main_order_id_seq'::regclass);


--
-- Name: main_orderitem id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_orderitem ALTER COLUMN id SET DEFAULT nextval('public.main_orderitem_id_seq'::regclass);


--
-- Name: main_question id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_question ALTER COLUMN id SET DEFAULT nextval('public.main_question_id_seq'::regclass);


--
-- Name: main_section id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_section ALTER COLUMN id SET DEFAULT nextval('public.main_section_id_seq'::regclass);


--
-- Name: main_subject id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_subject ALTER COLUMN id SET DEFAULT nextval('public.main_subject_id_seq'::regclass);


--
-- Name: main_topic id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_topic ALTER COLUMN id SET DEFAULT nextval('public.main_topic_id_seq'::regclass);


--
-- Name: main_transaction id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_transaction ALTER COLUMN id SET DEFAULT nextval('public.main_transaction_id_seq'::regclass);


--
-- Name: main_variant id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_variant ALTER COLUMN id SET DEFAULT nextval('public.main_variant_id_seq'::regclass);


--
-- Name: main_withdraw id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_withdraw ALTER COLUMN id SET DEFAULT nextval('public.main_withdraw_id_seq'::regclass);


--
-- Name: payment_paycomtransaction id; Type: DEFAULT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.payment_paycomtransaction ALTER COLUMN id SET DEFAULT nextval('public.payment_paycomtransaction_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add user	6	add_customuser
22	Can change user	6	change_customuser
23	Can delete user	6	delete_customuser
24	Can view user	6	view_customuser
25	Can add chat	7	add_chat
26	Can change chat	7	change_chat
27	Can delete chat	7	delete_chat
28	Can view chat	7	view_chat
29	Can add Class	8	add_class
30	Can change Class	8	change_class
31	Can delete Class	8	delete_class
32	Can view Class	8	view_class
33	Can add order	9	add_order
34	Can change order	9	change_order
35	Can delete order	9	delete_order
36	Can view order	9	view_order
37	Can add question	10	add_question
38	Can change question	10	change_question
39	Can delete question	10	delete_question
40	Can view question	10	view_question
41	Can add section	11	add_section
42	Can change section	11	change_section
43	Can delete section	11	delete_section
44	Can view section	11	view_section
45	Can add subject	12	add_subject
46	Can change subject	12	change_subject
47	Can delete subject	12	delete_subject
48	Can view subject	12	view_subject
49	Can add withdraw	13	add_withdraw
50	Can change withdraw	13	change_withdraw
51	Can delete withdraw	13	delete_withdraw
52	Can view withdraw	13	view_withdraw
53	Can add variant	14	add_variant
54	Can change variant	14	change_variant
55	Can delete variant	14	delete_variant
56	Can view variant	14	view_variant
57	Can add transaction	15	add_transaction
58	Can change transaction	15	change_transaction
59	Can delete transaction	15	delete_transaction
60	Can view transaction	15	view_transaction
61	Can add topic	16	add_topic
62	Can change topic	16	change_topic
63	Can delete topic	16	delete_topic
64	Can view topic	16	view_topic
65	Can add order item	17	add_orderitem
66	Can change order item	17	change_orderitem
67	Can delete order item	17	delete_orderitem
68	Can view order item	17	view_orderitem
69	Can add group	18	add_group
70	Can change group	18	change_group
71	Can delete group	18	delete_group
72	Can view group	18	view_group
73	Can add paycom transaction	19	add_paycomtransaction
74	Can change paycom transaction	19	change_paycomtransaction
75	Can delete paycom transaction	19	delete_paycomtransaction
76	Can view paycom transaction	19	view_paycomtransaction
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	main	customuser
7	main	chat
8	main	class
9	main	order
10	main	question
11	main	section
12	main	subject
13	main	withdraw
14	main	variant
15	main	transaction
16	main	topic
17	main	orderitem
18	main	group
19	payment	paycomtransaction
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2020-12-17 14:31:09.306401+00
2	contenttypes	0002_remove_content_type_name	2020-12-17 14:31:09.341326+00
3	auth	0001_initial	2020-12-17 14:31:09.462419+00
4	auth	0002_alter_permission_name_max_length	2020-12-17 14:31:09.648714+00
5	auth	0003_alter_user_email_max_length	2020-12-17 14:31:09.668048+00
6	auth	0004_alter_user_username_opts	2020-12-17 14:31:09.687794+00
7	auth	0005_alter_user_last_login_null	2020-12-17 14:31:09.706477+00
8	auth	0006_require_contenttypes_0002	2020-12-17 14:31:09.715389+00
9	auth	0007_alter_validators_add_error_messages	2020-12-17 14:31:09.734292+00
10	auth	0008_alter_user_username_max_length	2020-12-17 14:31:09.755931+00
11	auth	0009_alter_user_last_name_max_length	2020-12-17 14:31:09.775477+00
12	auth	0010_alter_group_name_max_length	2020-12-17 14:31:09.796952+00
13	auth	0011_update_proxy_permissions	2020-12-17 14:31:09.819188+00
14	auth	0012_alter_user_first_name_max_length	2020-12-17 14:31:09.839204+00
15	main	0001_initial	2020-12-17 14:31:11.133502+00
16	admin	0001_initial	2020-12-17 14:31:13.519853+00
17	admin	0002_logentry_remove_auto_add	2020-12-17 14:31:13.818844+00
18	admin	0003_logentry_add_action_flag_choices	2020-12-17 14:31:13.867557+00
19	payment	0001_initial	2020-12-17 14:31:13.952194+00
20	sessions	0001_initial	2020-12-17 14:31:14.060039+00
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
ejthi2frc8pyh0s6rf244vjuhke1ry72	.eJxVjMEOwiAQRP-FsyGwdFvw6L3fQGBZpGpoUtqT8d9tkx70Npn3Zt7Ch20tfmu8-CmJq9Di8tvFQE-uB0iPUO-zpLmuyxTlociTNjnOiV-30_07KKGVfe0IFJkEgNGqwRo92JD3gJ1xmDI5gOi4J0VKK-wcE5BGl4GJuUcjPl-_yzdH:1kpuK3:9xZDcgPLSMV-vsRSlIqJQzud1M8Y93djEnp4-wQiSJ0	2020-12-31 14:32:03.848312+00
\.


--
-- Data for Name: main_chat; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_chat (id, user_type, chat_id, first_name, last_name, username, phone, is_active, selected_test_format, would_like_mandatory_tests, step, balance, page, payment_amount, user_id) FROM stdin;
1	entrant	119438448	\N	\N	\N	\N	t	state_center_test	f	block_subjects	0	0	0	\N
\.


--
-- Data for Name: main_chat_groups; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_chat_groups (id, chat_id, group_id) FROM stdin;
\.


--
-- Data for Name: main_chat_selected_classes; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_chat_selected_classes (id, chat_id, class_id) FROM stdin;
\.


--
-- Data for Name: main_chat_selected_subjects; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_chat_selected_subjects (id, chat_id, subject_id) FROM stdin;
\.


--
-- Data for Name: main_chat_selected_topics; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_chat_selected_topics (id, chat_id, topic_id) FROM stdin;
\.


--
-- Data for Name: main_class; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_class (id, title) FROM stdin;
1	5
2	6
3	7
4	8
5	9
6	10
7	11
\.


--
-- Data for Name: main_customuser; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_customuser (id, password, last_login, is_superuser, username, first_name, last_name, phone, email, is_staff, is_active, type_of_user, date_joined, balance) FROM stdin;
1	pbkdf2_sha256$216000$MHF6jgcreEev$6XCS7fZZSqawm9eE11sdm4Ob8BUdW9XXlxRF+FlxpQA=	2020-12-17 14:32:03.837067+00	t	admin			\N	admin@edubot.uz	t	t	moderator	2020-12-17 14:31:38.639155+00	0
\.


--
-- Data for Name: main_customuser_groups; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_customuser_groups (id, customuser_id, group_id) FROM stdin;
\.


--
-- Data for Name: main_customuser_user_permissions; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_customuser_user_permissions (id, customuser_id, permission_id) FROM stdin;
\.


--
-- Data for Name: main_group; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_group (id, title, statistic) FROM stdin;
\.


--
-- Data for Name: main_group_students; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_group_students (id, group_id, chat_id) FROM stdin;
\.


--
-- Data for Name: main_order; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_order (created_at, id, total_price, file, is_paid, test_type, purchaser_id) FROM stdin;
\.


--
-- Data for Name: main_orderitem; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_orderitem (id, is_solved_correctly, price, order_id, question_id) FROM stdin;
\.


--
-- Data for Name: main_question; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_question (id, title, description, is_mandatory, is_state_center_test, level, is_owner, _class_id, owner_id, subject_id, topic_id) FROM stdin;
\.


--
-- Data for Name: main_section; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_section (id, title, subject_id) FROM stdin;
\.


--
-- Data for Name: main_subject; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_subject (id, title, is_thematic) FROM stdin;
1	Matematika	t
2	Ona tili	f
\.


--
-- Data for Name: main_topic; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_topic (id, title, _class_id, section_id, subject_id) FROM stdin;
\.


--
-- Data for Name: main_transaction; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_transaction (id, amount, created_at, payment_method, is_paid, owner_id) FROM stdin;
\.


--
-- Data for Name: main_variant; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_variant (id, title, description, is_correct, question_id) FROM stdin;
\.


--
-- Data for Name: main_withdraw; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.main_withdraw (id, status, summa, created_at, user_id) FROM stdin;
\.


--
-- Data for Name: payment_paycomtransaction; Type: TABLE DATA; Schema: public; Owner: ebot
--

COPY public.payment_paycomtransaction (id, paycom_transaction_id, paycom_time, paycom_time_datetime, create_time, amount, state, cancel_time, perform_time, reason, order_id) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 76, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 19, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 20, true);


--
-- Name: main_chat_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_chat_groups_id_seq', 1, false);


--
-- Name: main_chat_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_chat_id_seq', 1, true);


--
-- Name: main_chat_selected_classes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_chat_selected_classes_id_seq', 1, false);


--
-- Name: main_chat_selected_subjects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_chat_selected_subjects_id_seq', 1, false);


--
-- Name: main_chat_selected_topics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_chat_selected_topics_id_seq', 1, false);


--
-- Name: main_class_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_class_id_seq', 7, true);


--
-- Name: main_customuser_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_customuser_groups_id_seq', 1, false);


--
-- Name: main_customuser_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_customuser_id_seq', 1, true);


--
-- Name: main_customuser_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_customuser_user_permissions_id_seq', 1, false);


--
-- Name: main_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_group_id_seq', 1, false);


--
-- Name: main_group_students_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_group_students_id_seq', 1, false);


--
-- Name: main_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_order_id_seq', 1, false);


--
-- Name: main_orderitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_orderitem_id_seq', 1, false);


--
-- Name: main_question_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_question_id_seq', 1, false);


--
-- Name: main_section_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_section_id_seq', 1, false);


--
-- Name: main_subject_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_subject_id_seq', 2, true);


--
-- Name: main_topic_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_topic_id_seq', 1, false);


--
-- Name: main_transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_transaction_id_seq', 1, false);


--
-- Name: main_variant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_variant_id_seq', 1, false);


--
-- Name: main_withdraw_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.main_withdraw_id_seq', 1, false);


--
-- Name: payment_paycomtransaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: ebot
--

SELECT pg_catalog.setval('public.payment_paycomtransaction_id_seq', 1, false);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: main_chat_groups main_chat_groups_chat_id_group_id_d61b0b4c_uniq; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_groups
    ADD CONSTRAINT main_chat_groups_chat_id_group_id_d61b0b4c_uniq UNIQUE (chat_id, group_id);


--
-- Name: main_chat_groups main_chat_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_groups
    ADD CONSTRAINT main_chat_groups_pkey PRIMARY KEY (id);


--
-- Name: main_chat main_chat_phone_key; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat
    ADD CONSTRAINT main_chat_phone_key UNIQUE (phone);


--
-- Name: main_chat main_chat_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat
    ADD CONSTRAINT main_chat_pkey PRIMARY KEY (id);


--
-- Name: main_chat_selected_classes main_chat_selected_classes_chat_id_class_id_d66bc6a3_uniq; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_classes
    ADD CONSTRAINT main_chat_selected_classes_chat_id_class_id_d66bc6a3_uniq UNIQUE (chat_id, class_id);


--
-- Name: main_chat_selected_classes main_chat_selected_classes_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_classes
    ADD CONSTRAINT main_chat_selected_classes_pkey PRIMARY KEY (id);


--
-- Name: main_chat_selected_subjects main_chat_selected_subjects_chat_id_subject_id_767fda7f_uniq; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_subjects
    ADD CONSTRAINT main_chat_selected_subjects_chat_id_subject_id_767fda7f_uniq UNIQUE (chat_id, subject_id);


--
-- Name: main_chat_selected_subjects main_chat_selected_subjects_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_subjects
    ADD CONSTRAINT main_chat_selected_subjects_pkey PRIMARY KEY (id);


--
-- Name: main_chat_selected_topics main_chat_selected_topics_chat_id_topic_id_9c0148ac_uniq; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_topics
    ADD CONSTRAINT main_chat_selected_topics_chat_id_topic_id_9c0148ac_uniq UNIQUE (chat_id, topic_id);


--
-- Name: main_chat_selected_topics main_chat_selected_topics_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_topics
    ADD CONSTRAINT main_chat_selected_topics_pkey PRIMARY KEY (id);


--
-- Name: main_chat main_chat_user_id_key; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat
    ADD CONSTRAINT main_chat_user_id_key UNIQUE (user_id);


--
-- Name: main_class main_class_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_class
    ADD CONSTRAINT main_class_pkey PRIMARY KEY (id);


--
-- Name: main_customuser_groups main_customuser_groups_customuser_id_group_id_8a5023dd_uniq; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser_groups
    ADD CONSTRAINT main_customuser_groups_customuser_id_group_id_8a5023dd_uniq UNIQUE (customuser_id, group_id);


--
-- Name: main_customuser_groups main_customuser_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser_groups
    ADD CONSTRAINT main_customuser_groups_pkey PRIMARY KEY (id);


--
-- Name: main_customuser main_customuser_phone_key; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser
    ADD CONSTRAINT main_customuser_phone_key UNIQUE (phone);


--
-- Name: main_customuser main_customuser_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser
    ADD CONSTRAINT main_customuser_pkey PRIMARY KEY (id);


--
-- Name: main_customuser_user_permissions main_customuser_user_per_customuser_id_permission_06a652d8_uniq; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser_user_permissions
    ADD CONSTRAINT main_customuser_user_per_customuser_id_permission_06a652d8_uniq UNIQUE (customuser_id, permission_id);


--
-- Name: main_customuser_user_permissions main_customuser_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser_user_permissions
    ADD CONSTRAINT main_customuser_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: main_customuser main_customuser_username_key; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser
    ADD CONSTRAINT main_customuser_username_key UNIQUE (username);


--
-- Name: main_group main_group_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_group
    ADD CONSTRAINT main_group_pkey PRIMARY KEY (id);


--
-- Name: main_group_students main_group_students_group_id_chat_id_ea05252c_uniq; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_group_students
    ADD CONSTRAINT main_group_students_group_id_chat_id_ea05252c_uniq UNIQUE (group_id, chat_id);


--
-- Name: main_group_students main_group_students_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_group_students
    ADD CONSTRAINT main_group_students_pkey PRIMARY KEY (id);


--
-- Name: main_order main_order_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_order
    ADD CONSTRAINT main_order_pkey PRIMARY KEY (id);


--
-- Name: main_orderitem main_orderitem_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_orderitem
    ADD CONSTRAINT main_orderitem_pkey PRIMARY KEY (id);


--
-- Name: main_question main_question_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_question
    ADD CONSTRAINT main_question_pkey PRIMARY KEY (id);


--
-- Name: main_section main_section_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_section
    ADD CONSTRAINT main_section_pkey PRIMARY KEY (id);


--
-- Name: main_subject main_subject_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_subject
    ADD CONSTRAINT main_subject_pkey PRIMARY KEY (id);


--
-- Name: main_topic main_topic_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_topic
    ADD CONSTRAINT main_topic_pkey PRIMARY KEY (id);


--
-- Name: main_transaction main_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_transaction
    ADD CONSTRAINT main_transaction_pkey PRIMARY KEY (id);


--
-- Name: main_variant main_variant_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_variant
    ADD CONSTRAINT main_variant_pkey PRIMARY KEY (id);


--
-- Name: main_withdraw main_withdraw_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_withdraw
    ADD CONSTRAINT main_withdraw_pkey PRIMARY KEY (id);


--
-- Name: payment_paycomtransaction payment_paycomtransaction_pkey; Type: CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.payment_paycomtransaction
    ADD CONSTRAINT payment_paycomtransaction_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: main_chat_groups_chat_id_f3610808; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_chat_groups_chat_id_f3610808 ON public.main_chat_groups USING btree (chat_id);


--
-- Name: main_chat_groups_group_id_fa0ebfa2; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_chat_groups_group_id_fa0ebfa2 ON public.main_chat_groups USING btree (group_id);


--
-- Name: main_chat_phone_5251b9b2_like; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_chat_phone_5251b9b2_like ON public.main_chat USING btree (phone varchar_pattern_ops);


--
-- Name: main_chat_selected_classes_chat_id_70a916ee; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_chat_selected_classes_chat_id_70a916ee ON public.main_chat_selected_classes USING btree (chat_id);


--
-- Name: main_chat_selected_classes_class_id_7edda39b; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_chat_selected_classes_class_id_7edda39b ON public.main_chat_selected_classes USING btree (class_id);


--
-- Name: main_chat_selected_subjects_chat_id_2f9a8ebc; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_chat_selected_subjects_chat_id_2f9a8ebc ON public.main_chat_selected_subjects USING btree (chat_id);


--
-- Name: main_chat_selected_subjects_subject_id_832fb765; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_chat_selected_subjects_subject_id_832fb765 ON public.main_chat_selected_subjects USING btree (subject_id);


--
-- Name: main_chat_selected_topics_chat_id_22bbdbd7; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_chat_selected_topics_chat_id_22bbdbd7 ON public.main_chat_selected_topics USING btree (chat_id);


--
-- Name: main_chat_selected_topics_topic_id_895454e4; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_chat_selected_topics_topic_id_895454e4 ON public.main_chat_selected_topics USING btree (topic_id);


--
-- Name: main_customuser_groups_customuser_id_13869e25; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_customuser_groups_customuser_id_13869e25 ON public.main_customuser_groups USING btree (customuser_id);


--
-- Name: main_customuser_groups_group_id_8149f607; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_customuser_groups_group_id_8149f607 ON public.main_customuser_groups USING btree (group_id);


--
-- Name: main_customuser_phone_2eba86be_like; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_customuser_phone_2eba86be_like ON public.main_customuser USING btree (phone varchar_pattern_ops);


--
-- Name: main_customuser_user_permissions_customuser_id_34d37f86; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_customuser_user_permissions_customuser_id_34d37f86 ON public.main_customuser_user_permissions USING btree (customuser_id);


--
-- Name: main_customuser_user_permissions_permission_id_38e6f657; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_customuser_user_permissions_permission_id_38e6f657 ON public.main_customuser_user_permissions USING btree (permission_id);


--
-- Name: main_customuser_username_5ce75d5c_like; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_customuser_username_5ce75d5c_like ON public.main_customuser USING btree (username varchar_pattern_ops);


--
-- Name: main_group_students_chat_id_50e6d1d8; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_group_students_chat_id_50e6d1d8 ON public.main_group_students USING btree (chat_id);


--
-- Name: main_group_students_group_id_26a8ab06; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_group_students_group_id_26a8ab06 ON public.main_group_students USING btree (group_id);


--
-- Name: main_order_purchaser_id_ce412341; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_order_purchaser_id_ce412341 ON public.main_order USING btree (purchaser_id);


--
-- Name: main_orderitem_order_id_72ea9725; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_orderitem_order_id_72ea9725 ON public.main_orderitem USING btree (order_id);


--
-- Name: main_orderitem_question_id_013e295b; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_orderitem_question_id_013e295b ON public.main_orderitem USING btree (question_id);


--
-- Name: main_question__class_id_988f76fa; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_question__class_id_988f76fa ON public.main_question USING btree (_class_id);


--
-- Name: main_question_owner_id_73dc4f54; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_question_owner_id_73dc4f54 ON public.main_question USING btree (owner_id);


--
-- Name: main_question_subject_id_5556b103; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_question_subject_id_5556b103 ON public.main_question USING btree (subject_id);


--
-- Name: main_question_topic_id_7c3ee062; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_question_topic_id_7c3ee062 ON public.main_question USING btree (topic_id);


--
-- Name: main_section_subject_id_4ac93a1f; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_section_subject_id_4ac93a1f ON public.main_section USING btree (subject_id);


--
-- Name: main_topic__class_id_aa391c1e; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_topic__class_id_aa391c1e ON public.main_topic USING btree (_class_id);


--
-- Name: main_topic_section_id_e84489cf; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_topic_section_id_e84489cf ON public.main_topic USING btree (section_id);


--
-- Name: main_topic_subject_id_1e53ad3e; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_topic_subject_id_1e53ad3e ON public.main_topic USING btree (subject_id);


--
-- Name: main_transaction_owner_id_ea44961e; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_transaction_owner_id_ea44961e ON public.main_transaction USING btree (owner_id);


--
-- Name: main_variant_question_id_406e3ee8; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_variant_question_id_406e3ee8 ON public.main_variant USING btree (question_id);


--
-- Name: main_withdraw_user_id_b14e1f37; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX main_withdraw_user_id_b14e1f37 ON public.main_withdraw USING btree (user_id);


--
-- Name: payment_paycomtransaction_order_id_8061c1ba; Type: INDEX; Schema: public; Owner: ebot
--

CREATE INDEX payment_paycomtransaction_order_id_8061c1ba ON public.payment_paycomtransaction USING btree (order_id);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_main_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_main_customuser_id FOREIGN KEY (user_id) REFERENCES public.main_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_chat_groups main_chat_groups_chat_id_f3610808_fk_main_chat_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_groups
    ADD CONSTRAINT main_chat_groups_chat_id_f3610808_fk_main_chat_id FOREIGN KEY (chat_id) REFERENCES public.main_chat(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_chat_groups main_chat_groups_group_id_fa0ebfa2_fk_main_group_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_groups
    ADD CONSTRAINT main_chat_groups_group_id_fa0ebfa2_fk_main_group_id FOREIGN KEY (group_id) REFERENCES public.main_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_chat_selected_classes main_chat_selected_classes_chat_id_70a916ee_fk_main_chat_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_classes
    ADD CONSTRAINT main_chat_selected_classes_chat_id_70a916ee_fk_main_chat_id FOREIGN KEY (chat_id) REFERENCES public.main_chat(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_chat_selected_classes main_chat_selected_classes_class_id_7edda39b_fk_main_class_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_classes
    ADD CONSTRAINT main_chat_selected_classes_class_id_7edda39b_fk_main_class_id FOREIGN KEY (class_id) REFERENCES public.main_class(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_chat_selected_subjects main_chat_selected_s_subject_id_832fb765_fk_main_subj; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_subjects
    ADD CONSTRAINT main_chat_selected_s_subject_id_832fb765_fk_main_subj FOREIGN KEY (subject_id) REFERENCES public.main_subject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_chat_selected_subjects main_chat_selected_subjects_chat_id_2f9a8ebc_fk_main_chat_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_subjects
    ADD CONSTRAINT main_chat_selected_subjects_chat_id_2f9a8ebc_fk_main_chat_id FOREIGN KEY (chat_id) REFERENCES public.main_chat(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_chat_selected_topics main_chat_selected_topics_chat_id_22bbdbd7_fk_main_chat_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_topics
    ADD CONSTRAINT main_chat_selected_topics_chat_id_22bbdbd7_fk_main_chat_id FOREIGN KEY (chat_id) REFERENCES public.main_chat(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_chat_selected_topics main_chat_selected_topics_topic_id_895454e4_fk_main_topic_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat_selected_topics
    ADD CONSTRAINT main_chat_selected_topics_topic_id_895454e4_fk_main_topic_id FOREIGN KEY (topic_id) REFERENCES public.main_topic(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_chat main_chat_user_id_a26a681f_fk_main_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_chat
    ADD CONSTRAINT main_chat_user_id_a26a681f_fk_main_customuser_id FOREIGN KEY (user_id) REFERENCES public.main_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_customuser_groups main_customuser_grou_customuser_id_13869e25_fk_main_cust; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser_groups
    ADD CONSTRAINT main_customuser_grou_customuser_id_13869e25_fk_main_cust FOREIGN KEY (customuser_id) REFERENCES public.main_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_customuser_groups main_customuser_groups_group_id_8149f607_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser_groups
    ADD CONSTRAINT main_customuser_groups_group_id_8149f607_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_customuser_user_permissions main_customuser_user_customuser_id_34d37f86_fk_main_cust; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser_user_permissions
    ADD CONSTRAINT main_customuser_user_customuser_id_34d37f86_fk_main_cust FOREIGN KEY (customuser_id) REFERENCES public.main_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_customuser_user_permissions main_customuser_user_permission_id_38e6f657_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_customuser_user_permissions
    ADD CONSTRAINT main_customuser_user_permission_id_38e6f657_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_group_students main_group_students_chat_id_50e6d1d8_fk_main_chat_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_group_students
    ADD CONSTRAINT main_group_students_chat_id_50e6d1d8_fk_main_chat_id FOREIGN KEY (chat_id) REFERENCES public.main_chat(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_group_students main_group_students_group_id_26a8ab06_fk_main_group_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_group_students
    ADD CONSTRAINT main_group_students_group_id_26a8ab06_fk_main_group_id FOREIGN KEY (group_id) REFERENCES public.main_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_order main_order_purchaser_id_ce412341_fk_main_chat_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_order
    ADD CONSTRAINT main_order_purchaser_id_ce412341_fk_main_chat_id FOREIGN KEY (purchaser_id) REFERENCES public.main_chat(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_orderitem main_orderitem_order_id_72ea9725_fk_main_order_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_orderitem
    ADD CONSTRAINT main_orderitem_order_id_72ea9725_fk_main_order_id FOREIGN KEY (order_id) REFERENCES public.main_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_orderitem main_orderitem_question_id_013e295b_fk_main_question_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_orderitem
    ADD CONSTRAINT main_orderitem_question_id_013e295b_fk_main_question_id FOREIGN KEY (question_id) REFERENCES public.main_question(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_question main_question__class_id_988f76fa_fk_main_class_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_question
    ADD CONSTRAINT main_question__class_id_988f76fa_fk_main_class_id FOREIGN KEY (_class_id) REFERENCES public.main_class(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_question main_question_owner_id_73dc4f54_fk_main_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_question
    ADD CONSTRAINT main_question_owner_id_73dc4f54_fk_main_customuser_id FOREIGN KEY (owner_id) REFERENCES public.main_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_question main_question_subject_id_5556b103_fk_main_subject_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_question
    ADD CONSTRAINT main_question_subject_id_5556b103_fk_main_subject_id FOREIGN KEY (subject_id) REFERENCES public.main_subject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_question main_question_topic_id_7c3ee062_fk_main_topic_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_question
    ADD CONSTRAINT main_question_topic_id_7c3ee062_fk_main_topic_id FOREIGN KEY (topic_id) REFERENCES public.main_topic(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_section main_section_subject_id_4ac93a1f_fk_main_subject_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_section
    ADD CONSTRAINT main_section_subject_id_4ac93a1f_fk_main_subject_id FOREIGN KEY (subject_id) REFERENCES public.main_subject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_topic main_topic__class_id_aa391c1e_fk_main_class_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_topic
    ADD CONSTRAINT main_topic__class_id_aa391c1e_fk_main_class_id FOREIGN KEY (_class_id) REFERENCES public.main_class(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_topic main_topic_section_id_e84489cf_fk_main_section_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_topic
    ADD CONSTRAINT main_topic_section_id_e84489cf_fk_main_section_id FOREIGN KEY (section_id) REFERENCES public.main_section(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_topic main_topic_subject_id_1e53ad3e_fk_main_subject_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_topic
    ADD CONSTRAINT main_topic_subject_id_1e53ad3e_fk_main_subject_id FOREIGN KEY (subject_id) REFERENCES public.main_subject(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_transaction main_transaction_owner_id_ea44961e_fk_main_chat_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_transaction
    ADD CONSTRAINT main_transaction_owner_id_ea44961e_fk_main_chat_id FOREIGN KEY (owner_id) REFERENCES public.main_chat(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_variant main_variant_question_id_406e3ee8_fk_main_question_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_variant
    ADD CONSTRAINT main_variant_question_id_406e3ee8_fk_main_question_id FOREIGN KEY (question_id) REFERENCES public.main_question(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: main_withdraw main_withdraw_user_id_b14e1f37_fk_main_customuser_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.main_withdraw
    ADD CONSTRAINT main_withdraw_user_id_b14e1f37_fk_main_customuser_id FOREIGN KEY (user_id) REFERENCES public.main_customuser(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: payment_paycomtransaction payment_paycomtransaction_order_id_8061c1ba_fk_main_order_id; Type: FK CONSTRAINT; Schema: public; Owner: ebot
--

ALTER TABLE ONLY public.payment_paycomtransaction
    ADD CONSTRAINT payment_paycomtransaction_order_id_8061c1ba_fk_main_order_id FOREIGN KEY (order_id) REFERENCES public.main_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

