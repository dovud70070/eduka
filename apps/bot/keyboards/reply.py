from telebot import types

from apps.bot import messages


def remove_keyboard():
    markdown = types.ReplyKeyboardRemove()
    return markdown


def ask_phone_keyboard(message):
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    contact_btn = types.KeyboardButton(messages.PHONE_LABEL, request_contact=True)
    markdown.add(contact_btn)
    return markdown


def ask_user_type_keyboard(message):
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    entrant_btn = types.KeyboardButton(messages.ENTRANT_LABEL)
    mentor_btn = types.KeyboardButton(messages.MENTOR_LABEL)
    markdown.add(entrant_btn, mentor_btn)
    return markdown


def entrant_keyboard(message):
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton("PDF"),
    )
    markdown.add(
        types.KeyboardButton(messages.TAKE_TEST_LABEL),
        types.KeyboardButton(messages.MY_RESULTS_LABEL)
    )
    markdown.add(
        types.KeyboardButton(messages.MY_BALANCE_LABEL),
        types.KeyboardButton(messages.CHECK_RESULTS_LABEL)
    )
    markdown.add(
        types.KeyboardButton(messages.MY_ORDERS_LABEL),
        types.KeyboardButton(messages.MY_SETTINGS_LABEL)
    )
    return markdown


def mentor_keyboard(message):
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.ADD_GROUP_LABEL),
        types.KeyboardButton(messages.MY_SETTINGS_LABEL)
    )
    markdown.add(types.KeyboardButton(messages.GO_BACK_LABEL))
    return markdown


def test_format_keyboard(message):
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.STATE_TEST_CENTER_BLOCK_TEST_LABEL),
        types.KeyboardButton(messages.SUBJECT_TESTS_LABEL)
    )
    markdown.add(
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown


def test_order_keyboard():
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.ORDER_LABEL)
    )
    markdown.add(
        types.KeyboardButton(messages.GO_HOME_LABEL),
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown


def order_confirm_block():
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.CONFIRM_LABEL),
        types.KeyboardButton(messages.CANCEL_LABEL),
    )
    markdown.add(
        types.KeyboardButton(messages.GO_HOME_LABEL),
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown


def payment_methods_keyboard():
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.PAYME_LABEL),
        types.KeyboardButton(messages.CLICK_LABEL),
    )
    markdown.add(
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown


def payment_cancel_keyboard():
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.CANCEL_PAYMENT_LABEL)
    )
    markdown.add(
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown


def payment_operations_keyboard():
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.FILL_BALANCE_LABEL),
        types.KeyboardButton(messages.MY_TRANSACTIONS_LABEL)
    )
    markdown.add(
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown


def settings_markup_keyboard():
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.CHANGE_PHONE_LABEL, request_contact=True),
        types.KeyboardButton(messages.CHANGE_NAME_LABEL)
    )
    markdown.add(
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown


def go_back_keyboard():
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown


def my_results_keyboard():
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.WEEKLY_LABEL),
        types.KeyboardButton(messages.MONTHLY_LABEL)
    )
    markdown.add(
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown




def students_supervision():
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.STUDENT_CONTROL_LABEL),
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown


def statistic():
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.GO_HOME_LABEL),
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown


def answer():
    markdown = types.ReplyKeyboardMarkup(resize_keyboard=True)
    markdown.add(
        types.KeyboardButton(messages.GO_BACK_LABEL)
    )
    return markdown
