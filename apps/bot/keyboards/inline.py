from telebot import types

from apps.bot import messages
from apps.main.models import Group


def subjects_block_keyboard(chat, subjects_queryset, callback_prefix):
    keyboard = types.InlineKeyboardMarkup()
    items = []
    for index, subject in enumerate(subjects_queryset):
        item = types.InlineKeyboardButton(text=subject.title, callback_data="{}{}".format(callback_prefix, subject.id))
        items.append(item)
        if len(items) == 2:
            keyboard.add(*items)
            items = []
        if (index + 1) == subjects_queryset.count() and len(items) > 0:
            keyboard.add(*items)
    keyboard.add(types.InlineKeyboardButton(text=messages.GO_BACK_LABEL, callback_data="go_back"))
    return keyboard


def mandatory_tests_block_keyboard():
    keyboard = types.InlineKeyboardMarkup()
    keyboard.row(
        types.InlineKeyboardButton(text="Ha", callback_data="i_want_mandatory_test"),
        types.InlineKeyboardButton(text="Yo'q", callback_data="i_do_not_want_mandatory_test"),
    )
    return keyboard


def sections_block_keyboard(chat, sections_queryset, callback_prefix):
    selected_subject = chat.selected_subjects.first()
    keyboard = types.InlineKeyboardMarkup()
    for section in sections_queryset:
        queryset = chat.selected_topics.filter(section_id=section.id)
        if queryset.exists():
            text = "✅ {} ({})".format(section.title, queryset.count())
        else:
            text = section.title
        keyboard.add(types.InlineKeyboardButton(text=text,
                                                callback_data="{}{}".format(callback_prefix, section.id)))

    selected_topics = chat.selected_topics.filter(subject_id=selected_subject.id)
    if selected_topics.count():
        keyboard.add(types.InlineKeyboardButton(text=messages.ORDER_LABEL, callback_data="confirm_order"))
    keyboard.add(
        types.InlineKeyboardButton(text=messages.GO_HOME_LABEL, callback_data="go_home"),
        types.InlineKeyboardButton(text=messages.GO_BACK_LABEL, callback_data="go_back")
    )
    return keyboard


def topics_block_keyboard(chat, topics_queryset, callback_prefix):
    selected_items = chat.selected_topics.values_list('id', flat=True)
    keyboard = types.InlineKeyboardMarkup()
    for topic in topics_queryset:
        text = topic.title
        if topic.id in selected_items:
            text = "✅ {}".format(text)
        keyboard.add(types.InlineKeyboardButton(text=text,
                                                callback_data="{}{}".format(callback_prefix, topic.id)))
    keyboard.add(types.InlineKeyboardButton(text=messages.GO_BACK_LABEL, callback_data="go_back"))
    return keyboard


def classes_block_keyboard(chat, classes_queryset, callback_prefix):
    keyboard = types.InlineKeyboardMarkup()
    items = []
    selected_subject = chat.selected_subjects.first()
    for index, _class in enumerate(classes_queryset):
        selected_topics = chat.selected_topics.filter(_class_id=_class.id, subject_id=selected_subject.id)
        if selected_topics.exists():
            text = "✅ {} ({})".format(_class.title, selected_topics.count())
        else:
            text = _class.title
        item = types.InlineKeyboardButton(text=text, callback_data="{}{}".format(callback_prefix, _class.id))
        items.append(item)
        if len(items) == 3:
            keyboard.add(*items)
            items = []
        if (index + 1) == classes_queryset.count() and len(items) > 0:
            keyboard.add(*items)
            items = []

    selected_topics = chat.selected_topics.filter(subject_id=selected_subject.id)
    if selected_topics.count():
        keyboard.add(types.InlineKeyboardButton(text=messages.ORDER_LABEL, callback_data="confirm_order"))
    keyboard.add(
        types.InlineKeyboardButton(text=messages.GO_HOME_LABEL, callback_data="go_home"),
        types.InlineKeyboardButton(text=messages.GO_BACK_LABEL, callback_data="go_back")
    )
    return keyboard


def next_topics_block_keyboard(chat, topics_queryset, class_id):
    selected_items = chat.selected_topics.values_list('id', flat=True)
    keyboard = types.InlineKeyboardMarkup()
    items = []
    for index, topic in enumerate(topics_queryset):
        text = topic.title
        if topic.id in selected_items:
            text = "✅ {}".format(text)
        item = types.InlineKeyboardButton(text=text, callback_data="class_{}_topic_{}".format(class_id, topic.id))
        items.append(item)
        if len(items) == 3:
            keyboard.add(*items)
            items = []
        elif (index + 1) == topics_queryset.count() and len(items) > 0:
            keyboard.add(*items)
            items = []
    keyboard.add(
        types.InlineKeyboardButton(text=messages.SELECT_ALL_TOPICS_LABEL,
                                   callback_data="select_all_topics_for_{}".format(class_id)),
        types.InlineKeyboardButton(text=messages.GO_BACK_LABEL, callback_data="go_back")
    )
    return keyboard


def get_order_pagination_keyboard(current_page=2, total_pages=5):
    prev_page = (current_page - 1) or 1
    next_page = current_page + 1
    keyboard = types.InlineKeyboardMarkup(row_width=total_pages + 2)
    page_items = []
    pages = range(1, total_pages + 1)
    page_items.append(types.InlineKeyboardButton(text="<", callback_data="order_page_{}".format(prev_page)))
    for page in pages:
        item = types.InlineKeyboardButton(text=page, callback_data="order_page_{}".format(page))
        page_items.append(item)
    page_items.append(types.InlineKeyboardButton(text=">", callback_data="order_page_{}".format(next_page)))
    keyboard.add(*page_items)
    return keyboard


def payment_amounts_keyboard():
    keyboard = types.InlineKeyboardMarkup()
    items = []
    iterable = range(1000, 11000, 1000)
    for index, amount in enumerate(iterable):
        items.append(types.InlineKeyboardButton(text=amount, callback_data="amount_{}".format(amount)))
        if len(items) == 3 or len(iterable) == (index + 1):
            keyboard.add(*items)
            items = []
    return keyboard


def select_group(chat):
    groups = Group.objects.filter(chats__username=chat)
    keyboards = types.InlineKeyboardMarkup(row_width=3)
    items = []
    for group in groups:
        item = types.InlineKeyboardButton(text=group.title, callback_data="groups_{}".format(group.id))
        items.append(item)
    keyboards.add(*items)
    return keyboards


def statistic_group(group):
    keyboards = types.InlineKeyboardMarkup(row_width=2)
    items = [
        types.InlineKeyboardButton(text=messages.GROUP_STATISTICS_LABEL, callback_data='group_{}'.format(group.id)),
        types.InlineKeyboardButton(text=messages.STUDENT_LIST_LABEL, callback_data='group_{}'.format(group.id))]
    keyboards.add(*items)
    return keyboards


def send_test():
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    keyboard.row(
        types.InlineKeyboardButton(text=messages.I_APPROVE, callback_data="i_approve"),
    )
    return keyboard


def show_wrong_answer():
    keyboard = types.InlineKeyboardMarkup(row_width=2)
    keyboard.row(
        types.InlineKeyboardButton(text=messages.SHOW_WRONG_ANSWER, callback_data="show_wrong_answer")
    )
    return keyboard
