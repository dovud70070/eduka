import logging

import pdfkit

from django.conf import settings
from django.template.loader import render_to_string
from django.utils.timezone import now

from apps.main.models import Question, OrderItem, Order, Subject, ExtraData, OrderItemQuestion

__all__ = ['generate_order', 'get_pre_order_total_price', ]

from apps.main.templatetags.pdf_extras import mml_replace_with_svg


def select_questions_for(chat):
    already_purchased_questions = OrderItem.objects.filter(order__purchaser=chat).values_list('question_id', flat=True)
    selected_subjects = chat.selected_subjects.all()
    questions = Question.objects.exclude(pk__in=already_purchased_questions)
    if chat.selected_test_format == chat.STATE_CENTER_TEST:
        """
        DTM blok testi:
        1. 2 ta tanlangan fan bo'yicha -> 30 tadan 60 ta
        2. agar majburiy fan tanlangan bo'lsa, unda majburiy fanlar bo'yicha:  matematika, ona tili va tarix
        (agar birinchi 2 ta fanlarda majburiy fanlar tanlangan bo'lsa, unda tushurib qoldiriladi)
        """
        first_subject = selected_subjects[0]
        second_subject = selected_subjects[1]
        questions_list = {
                'first_subject': first_subject,
                'first_subject_questions': questions.filter(subject=first_subject, is_state_center_test=True).order_by("?")[0:30],
                'second_subject': second_subject,
                'second_subject_questions': questions.filter(subject=second_subject, is_state_center_test=True).order_by("?")[0:30]
            }
        if chat.would_like_mandatory_tests:
            """ 3 ta fan bo'yicha test qo'shish """
            mandatory_subjects = Subject.objects.filter(is_mandatory_block_subject=True)
            mandatory_questions = []
            for mandatory_subject in mandatory_subjects:
                mandatory_subject_questions = questions.filter(is_mandatory=True, subject=mandatory_subject)[0:15]
                mandatory_questions.append({
                    'subject': mandatory_subject.title,
                    'questions': mandatory_subject_questions
                })

            questions_list.update({
                'mandatory_questions': mandatory_questions
            })
    else:
        """
        Fanlar bo’yicha test:
        1. tanlangan fan, bo'lim va mavzular bo'yicha
        2. tanlangan fan, sinflar va mavzular bo'yicha
        """
        questions.filter(subject__in=selected_subjects)

    if chat.selected_test_format == chat.STATE_CENTER_TEST:
        pass
    else:
        pass
    return selected_subjects


def generate_order(chat):
    total_price = settings.TOTAL_PRICE
    order = Order(purchaser=chat, is_paid=False, total_price=total_price, created_at=now(),
                  test_type=chat.selected_test_format)
    order.save()
    if chat.selected_test_format == ExtraData.STATE_CENTER_TEST:
        selected_subjects = chat.selected_subjects.all()
        for subject in selected_subjects:
            order_item = OrderItem(subject=subject, order=order)
            order_item.save()

            selected_questions = subject.questions.only('id').order_by("?")[0:30]
            print(selected_questions.count())
            for question in selected_questions:
                ordered_question_item = OrderItemQuestion(order_item=order_item, question_id=question.id)
                ordered_question_item.save()
        if chat.would_like_mandatory_tests:
            mandatory_subjects = Subject.objects.filter(is_mandatory_block_subject=True)
            for subject in mandatory_subjects:
                order_item = OrderItem(subject=subject, order=order)
                order_item.save()
                selected_questions = order_item.subject.questions.all().order_by("?")[0:15]
                for question in selected_questions:
                    ordered_question_item = OrderItemQuestion(order_item=order_item, question=question)
                    ordered_question_item.save()
    else:
        pass
    order.refresh_from_db()
    return order


def get_pdf(order):
    file_name = 'orders/{}.pdf'.format(order.id)

    if order.test_type == ExtraData.STATE_CENTER_TEST:
        output = render_to_string('pdf/dtm.html', context={'order': order})
    else:
        output = render_to_string('pdf/subjected_test.html', context={'order': order})
    print(output)
    source = mml_replace_with_svg(output)
    is_created = pdfkit.from_string(source, 'media/{}'.format(file_name))

    if is_created:
        order.file.name = file_name
        order.save()
        return file_name
    return None


def get_pre_order_total_price(chat):
    return settings.TOTAL_PRICE
