import logging
import re
import time
import telebot
from telebot import types
from rest_framework.views import APIView
from rest_framework.response import Response

from django.shortcuts import render
from django.conf import settings

from apps.bot import messages
from apps.bot.keyboards import reply, inline
from apps.bot.services import get_pdf
from apps.bot.utils import *
from apps.bot.services import *
from apps.main.models import *

# bot = telebot.TeleBot(token=settings.TG_TOKEN, threaded=False)
TOKEN = "1541808588:AAH2_L3Y9nfHLUVonhSNLi_2Ft5QY1r-Aus"
bot = telebot.TeleBot(token=TOKEN, threaded=False)

page_size = 12


class BotView(APIView):

    def get(self, request, *args, **kwargs):
        try:
            bot.remove_webhook()
            time.sleep(0.1)
            bot.set_webhook(url="{}".format(settings.WEBHOOK_URL))
        except Exception as e:
            logging.exception(msg=str(e), exc_info=True)
        return render(request, "bot.html")

    def post(self, request, *args, **kwargs):
        if request.META.get('CONTENT_TYPE') == 'application/json':
            json_string = request.body.decode('utf-8')
            update = types.Update.de_json(json_string)
            try:
                bot.process_new_updates([update])
            except Exception as e:
                logging.exception(msg=str(e), exc_info=True)
            return Response(status=200)
        else:
            return Response(status=400)


class PaymentConfirmView(APIView):
    def get(self, request, *args, **kwargs):
        transaction = Transaction.objects.get(pk=kwargs.get('pk'))
        transaction.owner.balance += transaction.amount
        transaction.is_paid = True
        transaction.owner.save()
        transaction.save()
        # send notification to the chat about payment_old
        bot.send_message(chat_id=transaction.owner.chat_id, text="Sizning to'lovingiz qabul qilindi!",
                         reply_markup=my_balance_handler(chat=transaction.owner))
        return Response(data="Payment confirmed")


@bot.message_handler(commands=['start'])
def send_welcome(message):
    # /start
    chat = get_chat(message)
    chat.clear()
    chat.set_step(chat.WELCOME_MENU)
    chat.first_name = getattr(message.from_user3, 'first_name', '')
    chat.last_name = getattr(message.from_user, 'last_name', '')
    chat.username = getattr(message.from_user, 'username', '')
    chat.save()
    text = messages.WELCOME_MSG.format(get_full_name(message.chat))
    bot.send_message(chat_id=message.chat.id, text=text, parse_mode="HTML",
                     reply_markup=reply.ask_phone_keyboard(message))


@bot.message_handler(content_types=['contact'])
def contact_handler(message):
    # 📞 Telefon raqam
    chat = get_chat(message)
    print(chat.step)
    if chat.step in [chat.MY_SETTINGS_MENU, chat.CHANGE_NAME_MENU]:
        chat.phone = message.contact.phone_number
        chat.save()
        bot.send_message(chat_id=message.chat.id, text=messages.PHONE_SUCCESSFUL_CHANGED)
    elif chat.step == chat.WELCOME_MENU:
        chat.phone = message.contact.phone_number
        chat.save()
        chat.set_step(chat.USER_TYPE_MENU)
        chat.save()
        bot.send_message(chat_id=message.chat.id, text=messages.REGISTER_TYPE,
                         reply_markup=reply.ask_user_type_keyboard(message))
    elif chat.step == chat.MENTOR_MENU:
        print('sdffds')
        chat.set_step(chat.USER_TYPE_MENU)
        chat.save()
        bot.send_message(chat_id=message.chat.id, text=messages.REGISTER_TYPE,
                         reply_markup=reply.ask_user_type_keyboard(message))
    else:
        bot.send_message(chat_id=message.chat.id, text=messages.UNKNOWN_MSG)


@bot.message_handler(regexp=f'(?:{messages.ENTRANT_LABEL})')
def entrant_handler(message):
    # ‍🎓 Abiturent
    chat = get_chat(message)
    chat.set_step(chat.ENTRANT_MENU)
    bot.send_message(chat_id=message.chat.id, text=messages.CHOOSE_RIGHT_MENU,
                     reply_markup=reply.entrant_keyboard(message))


@bot.message_handler(regexp=f'PDF')
def pdf_handler(message):
    # PDF
    chat = get_chat(message)
    bot.send_message(chat_id=message.chat.id, text=messages.CHOOSE_RIGHT_MENU)


@bot.message_handler(regexp=f'(?:{messages.MENTOR_LABEL})')
def mentor_handler(message):
    # 👨‍🏫 Ustoz
    chat = get_chat(message)
    chat.set_step(chat.MENTOR_MENU)
    markup = inline.select_group(chat=chat)
    bot.send_message(chat_id=message.chat.id, text=messages.SELECT_GROUP_LABEL,
                     reply_markup=markup)
    bot.send_message(chat_id=message.chat.id, text=messages.CHOOSE_RIGHT_MENU,
                     reply_markup=reply.mentor_keyboard(message))


@bot.message_handler(regexp=f'(?:{messages.TAKE_TEST_LABEL})')
def take_test_handler(message):
    # Test topshirish
    chat = get_chat(message)
    chat.clear()
    chat.set_step(chat.TEST_FORMAT_MENU)
    bot.send_message(chat_id=message.chat.id, text=messages.CHOOSE_TEST_FORMAT_MENU,
                     reply_markup=reply.test_format_keyboard(message))


@bot.message_handler(regexp=f'(?:{messages.STATE_TEST_CENTER_BLOCK_TEST_LABEL})')
def dtm_block_test_handler(message):
    # DTM blok testi
    chat = get_chat(message)
    chat.set_step(chat.BLOCK_SUBJECTS_MENU)
    chat.selected_subjects.clear()
    chat.selected_test_format = chat.STATE_CENTER_TEST
    chat.save()
    subjects = Subject.objects.all()
    sent_message = bot.send_message(chat_id=chat.chat_id, reply_markup=reply.remove_keyboard(),
                                    text=messages.FIRST_BLOCK_SUBJECTS_LABEL)
    bot.delete_message(chat_id=chat.chat_id, message_id=sent_message.message_id)
    markup = inline.subjects_block_keyboard(chat, subjects, "block_test_subject_1_")
    bot.send_message(chat_id=chat.chat_id, text=messages.FIRST_BLOCK_SUBJECTS_LABEL, reply_markup=markup)


@bot.message_handler(regexp=f'(?:{messages.GO_HOME_LABEL})')
def go_home_handler(message):
    # 🏠 Boshiga qaytish
    chat = get_chat(message)
    chat.clear()
    if chat.user_type == chat.ENTRANT:
        entrant_handler(message)
    else:
        select_group_handler(message)


@bot.message_handler(regexp=f'(?:{messages.ORDER_LABEL})')
def order_handler(message):
    # 🛒 Buyurtma berish
    chat = get_chat(message)
    chat.set_step(chat.BLOCK_SUBJECTS_ORDER_CONFIRM_MENU)
    text = f"<b>Testning umumiy bahosi: {get_pre_order_total_price(chat)}</b>"
    bot.send_message(chat_id=message.chat.id, text=text, parse_mode="HTML", reply_markup=reply.order_confirm_block())


@bot.message_handler(regexp=f'(?:{messages.CONFIRM_LABEL})')
def order_confirm_handler(message):
    # ✅ Tasdiqlash
    chat = get_chat(message)
    chat.set_step(chat.BLOCK_SUBJECTS_ORDER_FILE_MENU)
    order = generate_order(chat)
    if chat.balance < order.total_price:
        text = "👆 Balansingizda mablag' yetarli emas, iltimos balansingizni to'ldiring va qaytadan urinib ko'ring!"
        bot.send_message(chat.chat_id, text, reply_markup=reply.payment_operations_keyboard())
    else:
        chat.make_payment_for(order)
        pdf = get_pdf(order)
        text = "Buyurtmangiz uchun rahmat!\nBuyurtma raqami: <b>{}</b>".format(order.id)
        if order.file and order.file.name:
            bot.send_document(chat_id=chat.chat_id, parse_mode="HTML", reply_markup=reply.entrant_keyboard(message),
                              data=order.file, caption=text)
        else:
            bot.send_message(chat_id=chat.chat_id, text=text, parse_mode="HTML",
                             reply_markup=reply.entrant_keyboard(message))


@bot.message_handler(regexp=f'(?:{messages.CANCEL_LABEL})')
def order_cancel_handler(message):
    # ❌ Bekor qilish
    chat = get_chat(message)
    if chat.step == chat.BLOCK_SUBJECTS_ORDER_CONFIRM_MENU:
        go_home_handler(message)


@bot.callback_query_handler(func=lambda message: re.search(r"block_test_subject_1_\d+", message.data))
def dtm_block_test_subject_callback_query_handler(inline_query):
    # 1-blokni tanlang:
    message = inline_query.message
    subject_id = inline_query.data.replace("block_test_subject_1_", "")
    chat = get_chat(message)
    chat.set_step(chat.BLOCK_SUBJECTS_MENU)
    chat.selected_subjects.add(subject_id)
    chat.save()
    subjects = Subject.objects.exclude(id=subject_id)
    bot.edit_message_text(chat_id=message.chat.id, text=messages.SECOND_BLOCK_SUBJECTS_LABEL,
                          message_id=message.message_id,
                          reply_markup=inline.subjects_block_keyboard(chat, subjects, "block_test_subject_2_"))


@bot.callback_query_handler(func=lambda message: re.search(r"block_test_subject_2_\d+", message.data))
def dtm_block_test_second_subject_callback_query_handler(inline_query):
    # 2-blokni tanlang:
    message = inline_query.message
    subject_id = inline_query.data.replace("block_test_subject_2_", "")
    chat = get_chat(message)
    chat.set_step(chat.BLOCK_SUBJECTS_MENU)
    chat.selected_subjects.add(subject_id)
    chat.save()
    bot.edit_message_text(chat_id=message.chat.id, message_id=message.message_id,
                          text=messages.MANDATORY_TEST_BLOCK_LABEL,
                          reply_markup=inline.mandatory_tests_block_keyboard())


@bot.callback_query_handler(
    func=lambda message: message.data in ["i_want_mandatory_test", "i_do_not_want_mandatory_test"])
def dtm_ask_mandatory_test_callback_query_handler(inline_query):
    message = inline_query.message
    chat = get_chat(message)
    bot.delete_message(chat_id=chat.chat_id, message_id=message.message_id)
    if inline_query.data == "i_want_mandatory_test":
        chat.would_like_mandatory_tests = True
        would_like_mandatory_tests = "Ha"
    else:
        chat.would_like_mandatory_tests = False
        would_like_mandatory_tests = "Yo'q"
    text = ""
    for index, subject in enumerate(chat.selected_subjects.all()):
        text += "{}-blok: {}\n".format(index + 1, subject.title)
    text += "Majburiy blok: {}\n".format(would_like_mandatory_tests)
    bot.send_message(chat_id=message.chat.id, text=text, reply_markup=reply.test_order_keyboard())
    chat.save()


@bot.message_handler(regexp=f'(?:{messages.SUBJECT_TESTS_LABEL})')
def subjected_take_tests_handler(message):
    # Fanlar bo’yicha test
    chat = get_chat(message)
    chat.selected_test_format = chat.SUBJECTED_TEST
    chat.set_step(chat.CHOOSE_SUBJECTS_MENU)
    subjects = Subject.objects.all()
    reply_markup = inline.subjects_block_keyboard(chat, subjects, "subjected_test_subject_")
    sent_message = bot.send_message(chat_id=message.chat.id, reply_markup=reply.remove_keyboard(),
                                    text=messages.SECTIONS_LABEL)
    bot.delete_message(chat_id=sent_message.chat.id, message_id=sent_message.message_id)
    bot.send_message(chat_id=message.chat.id, reply_markup=reply_markup, text=messages.SECTIONS_LABEL)


@bot.callback_query_handler(func=lambda message: re.search(r"subjected_test_subject_\d+", message.data))
def subjected_test_subject_callback_query_handler(inline_query, subject_id=None):
    # Fanlar bo’yicha test -> Bo'limni tanlang(Bo'limlar)
    message = inline_query.message
    chat = get_chat(message)
    chat.set_step(chat.CHOOSE_SECTIONS_MENU)
    if subject_id is None:
        subject_id = inline_query.data.replace("subjected_test_subject_", "")
    subject = Subject.objects.get(pk=subject_id)
    chat.selected_subjects.clear()
    chat.selected_subjects.add(subject_id)
    chat.save()
    bot.delete_message(chat_id=chat.chat_id, message_id=message.message_id)
    if subject.is_thematic:
        # move to sections menu
        sections = Section.objects.filter(subject_id=subject_id, topics__isnull=False).distinct()
        bot.send_message(chat_id=message.chat.id, text=messages.CHOOSE_SECTIONS_MENU,
                         reply_markup=inline.sections_block_keyboard(chat, sections, "section_"))
    else:
        # move to classes menu
        classes = Class.objects.all()
        bot.send_message(chat_id=message.chat.id, text=messages.CHOOSE_CLASS_MENU,
                         reply_markup=inline.classes_block_keyboard(chat, classes, "class_"))


@bot.callback_query_handler(func=lambda message: re.search(r"class_\d+$", message.data))
def class_callback_query_handler(inline_query):
    # Sinfni tanlang -> Mavzuni tanlang:
    message = inline_query.message
    chat = get_chat(message)
    chat.set_step(chat.CHOOSE_TOPICS_MENU)
    class_id = inline_query.data.replace("class_", "")
    chat.selected_classes.add(class_id)
    chat.save()
    subject_id = chat.selected_subjects.first().id
    topics = Topic.objects.filter(_class_id=class_id, subject_id=subject_id)
    bot.edit_message_text(chat_id=message.chat.id, text=messages.CHOOSE_TOPIC_MENU, message_id=message.message_id,
                          reply_markup=inline.next_topics_block_keyboard(chat, topics, class_id))


@bot.callback_query_handler(func=lambda message: re.search(r"class_\d+_topic_\d+$", message.data))
def class_topic_callback_query_handler(inline_query):
    # Fanlar bo'yicha test -> Fanlar -> Sinflar -> Mavzuni tanlash
    message = inline_query.message
    chat = get_chat(message=message)
    chat.set_step(chat.CHOOSE_TOPICS_MENU)
    class_id = inline_query.data.split("_")[1]
    topic_id = int(inline_query.data.split("_")[3])
    if topic_id in chat.selected_topics.values_list('id', flat=True):
        chat.selected_topics.remove(topic_id)
    else:
        chat.selected_topics.add(topic_id)
    chat.save()
    subject_id = chat.selected_subjects.first().id
    topics = Topic.objects.filter(_class_id=class_id, subject_id=subject_id)
    bot.edit_message_text(chat_id=message.chat.id, text=messages.CHOOSE_TOPIC_MENU, message_id=message.message_id,
                          reply_markup=inline.next_topics_block_keyboard(chat, topics, class_id))


@bot.callback_query_handler(func=lambda message: re.search(r"select_all_topics_for_\d+$", message.data))
def class_select_all_topics_callback_query_handler(inline_query):
    # Fanlar bo'yicha test -> Fanlar -> Sinflar -> Mavzuni tanlash -> Barchasini tanlash
    message = inline_query.message
    chat = get_chat(message=message)
    chat.set_step(chat.CHOOSE_TOPICS_MENU)
    class_id = inline_query.data.split("_")[-1]
    subject_id = chat.selected_subjects.first().id
    topics = Topic.objects.filter(_class_id=class_id, subject_id=subject_id)
    selected_topics = chat.selected_topics.filter(_class_id=class_id, subject_id=subject_id)

    if selected_topics.count() == topics.count():
        for topic in selected_topics:
            chat.selected_topics.remove(topic.id)
    else:
        for topic in topics:
            chat.selected_topics.add(topic.id)
    chat.save()
    bot.edit_message_text(chat_id=message.chat.id, text=messages.CHOOSE_TOPIC_MENU, message_id=message.message_id,
                          reply_markup=inline.next_topics_block_keyboard(chat, topics, class_id))


@bot.callback_query_handler(func=lambda message: re.search(r"section_\d+$", message.data))
def section_callback_query_handler(inline_query):
    # Mavzuni tanlang:
    message = inline_query.message
    chat = get_chat(message)
    chat.set_step(chat.CHOOSE_TOPICS_MENU)
    section_id = inline_query.data.replace("section_", "")
    subject_id = chat.selected_subjects.first().id
    topics = Topic.objects.filter(section_id=section_id, subject_id=subject_id)
    callback_prefix = "section_{}_topic_".format(section_id)
    bot.edit_message_text(chat_id=message.chat.id, text=messages.CHOOSE_TOPIC_MENU, message_id=message.message_id,
                          reply_markup=inline.topics_block_keyboard(chat, topics, callback_prefix))


@bot.callback_query_handler(func=lambda message: re.search(r"section_\d+_topic_\d+$", message.data))
def topic_callback_query_handler(inline_query):
    # DTM -> Mavzuni tanlash
    message = inline_query.message
    chat = get_chat(message=message)
    chat.set_step(chat.CHOOSE_TOPICS_MENU)
    data = inline_query.data
    section_id = data.split("_")[1]
    topic_id = int(data.split("_")[3])
    if topic_id in chat.selected_topics.values_list('id', flat=True):
        chat.selected_topics.remove(topic_id)
    else:
        chat.selected_topics.add(topic_id)
    callback_prefix = "section_{}_topic_".format(section_id)
    subject_id = chat.selected_subjects.first().id
    topics = Topic.objects.filter(section_id=section_id, subject_id=subject_id)
    chat.save()
    reply_markup = inline.topics_block_keyboard(chat, topics, callback_prefix)
    bot.edit_message_text(chat_id=message.chat.id, message_id=message.message_id, reply_markup=reply_markup,
                          text=messages.CHOOSE_TOPIC_MENU)


@bot.callback_query_handler(func=lambda message: message.data == "go_home")
def go_home_handler(inline_query):
    # 🏠 Boshiga qaytish
    message = inline_query.message
    bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)
    take_test_handler(message)


@bot.callback_query_handler(func=lambda message: message.data == "confirm_order")
def confirm_order_handler(inline_query):
    # ◀ ️Orqaga
    message = inline_query.message
    chat = get_chat(message)
    selected_subject = chat.selected_subjects.first()
    text = "<b>Sizning buyurtmangiz</b>\n\n"
    text += "<b>Fan:</b> {}\n".format(selected_subject.title)
    topics = chat.selected_topics.filter(subject_id=selected_subject.id)
    text += "<b>Mavzular:</b> {} ta\n".format(topics.count())
    text += "<b>Narxi:</b> 1 200 so'm"
    bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)
    bot.send_message(chat_id=message.chat.id, text=text, parse_mode="HTML", reply_markup=reply.order_confirm_block())


@bot.message_handler(regexp=f'(?:{messages.MY_ORDERS_LABEL})')
def my_orders_handler(message):
    # 🛒 Buyurtmalarim
    chat = get_chat(message)
    chat.page = 1
    chat.set_step(chat.MY_ORDERS_MENU)
    my_orders = Order.objects.filter(purchaser=chat)
    end = page_size * (chat.page or 1)
    start = end - page_size
    total_pages = int(my_orders.count() / page_size) + 1
    my_orders = my_orders[start:end]
    if my_orders.exists():
        reply_markup = inline.get_order_pagination_keyboard(current_page=chat.page, total_pages=total_pages)
        text = get_orders_text(my_orders, chat)
        bot.send_message(chat_id=chat.chat_id, text=text, parse_mode="HTML", reply_markup=reply_markup)
    else:
        text = "🤭 Uzr, hozircha sizda buyurtmalar mavjud emas!"
        bot.send_message(chat_id=chat.chat_id, text=text, )


@bot.callback_query_handler(func=lambda message: re.search(r"order_page_\d+$", message.data))
def my_orders_paginate_handler(inline_query):
    # 🛒 Buyurtmalarim
    message = inline_query.message
    page = int(inline_query.data.split("_")[2])
    chat = get_chat(message)
    if chat.page != page:
        chat.page = page
        chat.save()
        my_orders = Order.objects.filter(purchaser=chat)
        end = page_size * (chat.page or 1)
        start = end - page_size
        total_pages = int(my_orders.count() / page_size) + 1
        if 0 < page <= total_pages:
            my_orders = my_orders[start:end]
            text = get_orders_text(my_orders, chat)
            bot.edit_message_text(chat_id=chat.chat_id, text=text, parse_mode="HTML", message_id=message.message_id,
                                  reply_markup=inline.get_order_pagination_keyboard(current_page=chat.page,
                                                                                    total_pages=total_pages))


@bot.message_handler(regexp=r'/o\d+$')
def order_detail_handler(message):
    # 🛒 Buyurtmalarim
    chat = get_chat(message)
    order_id = message.text.replace("/o", "")
    order = Order.objects.get(pk=order_id)
    text = get_order_detail(order)
    if order.file:
        bot.send_document(chat.chat_id, caption=text, data=order.file, reply_to_message_id=message.message_id)
    else:
        bot.reply_to(message, text)


@bot.message_handler(regexp=r'/pay\d+$')
def pay_for_order_handler(message):
    # Buyurtmaga to'lov
    chat = get_chat(message)
    order_id = message.text.replace("/pay", "")
    order = Order.objects.get(pk=order_id)
    if order.is_paid:
        text = get_order_detail(order)
        if order.file:
            bot.send_document(chat.chat_id, caption=text, data=order.file, reply_to_message_id=message.message_id)
        else:
            bot.send_message(chat.chat_id, text=text, reply_to_message_id=message.message_id)
    else:
        if chat.balance >= order.total_price:
            chat.make_payment_for(order)
            text = get_order_detail(order)
            try:
                bot.send_document(chat.chat_id, caption=text, data=order.file, reply_to_message_id=message.message_id)
            except Exception as e:
                print(str(e))
        else:
            text = "☹👆 Balansingizda mablag' yetarli emas, iltimos balansingizni to'ldiring va qaytadan urinib ko'ring!"
            bot.send_message(chat.chat_id, text, reply_markup=reply.payment_operations_keyboard())


@bot.message_handler(regexp=f'(?:{messages.MY_BALANCE_LABEL})')
def my_balance_handler(message=None, chat=None):
    # 💰 Mening hisobim
    if message:
        chat = get_chat(message)
    chat.set_step(chat.MY_BALANCE_MENU)
    text = "Sizning balansingizda: {} so'm\n".format(chat.balance)
    text += "Amaliyot turini tanlang"
    bot.send_message(chat_id=chat.chat_id, text=text, reply_markup=reply.payment_operations_keyboard(),
                     parse_mode="HTML")


@bot.message_handler(regexp=f'(?:{messages.FILL_BALANCE_LABEL})')
def filling_balance_handler(message):
    # Hisobni to’ldirish
    chat = get_chat(message)
    chat.set_step(chat.FILLING_BALANCE_MENU)
    text = "Marhamat to'lov miqdorini kiriting:"
    bot.send_message(chat_id=chat.chat_id, text=text, reply_markup=inline.payment_amounts_keyboard())


@bot.callback_query_handler(func=lambda message: re.search(r"amount_\d+$", message.data))
def payment_amounts_handler(inline_query):
    # To'lov miqdorini kiritish
    message = inline_query.message
    amount = inline_query.data.replace("amount_", "")
    chat = get_chat(message)
    chat.payment_amount = amount
    chat.set_step(chat.CHOOSE_PAYMENT_METHOD_MENU)
    bot.delete_message(chat.chat_id, message.message_id)
    bot.send_message(chat_id=chat.chat_id, text=messages.CHOOSE_PAYMENT_METHOD,
                     reply_markup=reply.payment_methods_keyboard())


@bot.message_handler(regexp=f'(?:{messages.PAYME_LABEL}|{messages.CLICK_LABEL})')
def payment_method_handler(message):
    # Payme/Click
    chat = get_chat(message)
    chat.set_step(chat.CHOOSE_PAYMENT_METHOD_MENU)
    transaction = Transaction()
    transaction.amount = chat.payment_amount
    transaction.owner = chat
    url = ''
    if message.text == messages.PAYME_LABEL:
        transaction.payment_method = transaction.PAYME
        transaction.save()
        url = f'{settings.WEBHOOK_URL}/payment_old/{transaction.id}'
    else:
        transaction.payment_method = transaction.CLICK
        transaction.save()
        url = f'{settings.WEBHOOK_URL}/payment_old/{transaction.id}'
    text = "Link: <a href='{0}'>{0}</a>".format(url)
    bot.send_message(chat.chat_id, text, reply_markup=reply.payment_cancel_keyboard(), parse_mode="HTML")


@bot.message_handler(regexp=f'(?:{messages.CANCEL_PAYMENT_LABEL})')
def cancel_payment_handler(message):
    # ❌ To'lovni bekor qilish
    chat = get_chat(message)
    Transaction.objects.filter(owner=chat, is_paid=False).delete()
    bot.send_message(chat.chat_id, text="To'lovingiz bekor qilindi")
    entrant_handler(message)


@bot.message_handler(regexp=f'(?:{messages.MY_TRANSACTIONS_LABEL})')
def my_transactions_handler(message):
    # Mening tranzaksiyalarim tarixi
    chat = get_chat(message)
    transactions = Transaction.objects.filter(owner=chat)
    if transactions.exists():
        text = get_transactions_text(transactions)
    else:
        text = "🤭 Uzr, sizda hozircha tranzaksiyalar mavjud emas!"
    bot.send_message(chat.chat_id, text, parse_mode="HTML")


@bot.message_handler(regexp=f'(?:{messages.CHANGE_NAME_LABEL})')
def change_name_handler(message):
    # F.I.O.ni yuboring
    chat = get_chat(message)
    chat.set_step(chat.CHANGE_NAME_MENU)
    text = messages.CHANGE_NAME_MSG
    bot.send_message(chat.chat_id, text)


@bot.message_handler(regexp=f'(?:{messages.MY_SETTINGS_LABEL})')
def my_settings_handler(message):
    # 🔧 Sozlamalarim
    chat = get_chat(message)
    chat.set_step(chat.MY_SETTINGS_MENU)
    text = "Nimani o'zgartirmoqchisiz ?"
    bot.send_message(chat.chat_id, text=text, reply_markup=reply.settings_markup_keyboard())


@bot.message_handler(regexp=f'(?:{messages.MY_RESULTS_LABEL})')
def my_results_handler(message):
    # 📈 Natijalarim
    chat = get_chat(message)
    chat.set_step(chat.MY_RESULTS_MENU)
    text = messages.MY_RESULTS_MSG
    bot.send_message(chat.chat_id, text=text, reply_markup=reply.my_results_keyboard())


@bot.message_handler(regexp=f'(?:{messages.WEEKLY_LABEL}|{messages.MONTHLY_LABEL})')
def my_results_report_handler(message):
    chat = get_chat(message)
    if message.text == messages.WEEKLY_LABEL:
        pass
    else:
        pass
    with open(settings.BASE_DIR / "media/resume.pdf", "rb") as f:
        bot.send_document(chat.chat_id, data=f)


@bot.message_handler(regexp=f'(?:{messages.CHECK_RESULTS_LABEL})')
def check_results_handler(message):
    chat = get_chat(message)
    chat.step = chat.CHECK_RESULT
    chat.save()
    print(chat)
    text = messages.CHECK_RESULTS_FORMAT_MSG
    bot.send_message(chat.chat_id, text=text, reply_markup=reply.answer())


@bot.message_handler(regexp=f'(?:{messages.GO_BACK_LABEL})')
def go_back_handler(message):
    # ◀ ️Orqaga
    chat = get_chat(message)
    print(chat.step)
    if chat.step == chat.BLOCK_SUBJECTS_MENU:
        take_test_handler(message)
    elif chat.step == chat.BLOCK_SUBJECTS_ORDER_CONFIRM_MENU:
        dtm_block_test_handler(message)
    elif chat.step == chat.CHOOSE_PAYMENT_METHOD_MENU:
        my_balance_handler(message)
    elif chat.step == chat.MENTOR_MENU:
        print(chat.MENTOR_MENU)
        contact_handler(message)
    elif chat.step == chat.ADD_GROUP and chat.user_type == chat.MENTOR:
        print(chat.step)
        select_group_handler(message)
    elif chat.step == chat.MY_SETTINGS_MENU and chat.user_type == chat.MENTOR:
        select_group_handler(message)
    elif chat.step == chat.SELECTED_GROUP:
        select_group_handler(message)
    elif chat.step == chat.CHANGE_NAME_MENU and chat.user_type == chat.MENTOR:
        select_group_handler(message)
    else:
        entrant_handler(message)


# select_operation

@bot.message_handler(regexp=f'(?:{messages.MENTOR_LABEL})')
def select_group_handler(message):
    # 👨‍🏫 Ustoz menu
    chat = get_chat(message)
    chat.user_type = chat.MENTOR
    chat.set_step(chat.MENTOR_MENU)
    if chat.step == chat.MENTOR:
        markup = inline.select_group(chat=chat)
        bot.send_message(chat_id=message.chat.id, text=messages.SELECT_GROUP_LABEL,
                         reply_markup=markup)
        bot.send_message(chat_id=message.chat.id, text=messages.CHOOSE_RIGHT_MENU,
                         reply_markup=reply.mentor_keyboard(message=message))


@bot.message_handler(regexp=f'(?:{messages.ADD_GROUP_LABEL})')
def select_operation(message):
    # Guruh qo'shish havola
    chat = get_chat(message)
    chat.set_step(chat.ADD_GROUP)

    chat.save()
    bot.delete_message(chat_id=message.chat.id, message_id=message.id)
    bot.send_message(chat_id=message.chat.id, text=messages.ADD_GROUP, reply_markup=reply.go_back_keyboard())


@bot.message_handler(regexp=f'(?:{messages.STUDENT_CONTROL_LABEL})')
def select_operation_control(message):
    chat = get_chat(message)
    chat.set_step(chat.STUDEND_CONTROL)
    bot.delete_message(chat_id=message.chat.id, message_id=message.id - 1)
    bot.delete_message(chat_id=message.chat.id, message_id=message.id)
    bot.send_message(chat_id=message.chat.id, text=messages.STUDENT_CONTROL, reply_markup=reply.go_back_keyboard())


@bot.message_handler(regexp=r'(\d+)#(\w+)')
def handle_check_user_solution(message):
    data = message.text
    chat = get_chat(message)
    chat.answer = data
    chat.save()

    order_id, answers = data.split('#')
    answers = list(answers)
    text = ''
    order_items = OrderItem.objects.filter(order_id=order_id)
    order_purchaser_chat_id = order_items.first().order.purchaser.chat_id
    if order_items.count() == len(answers) and order_purchaser_chat_id == chat.chat_id:
        for index, value in enumerate(answers):
            text += "{}.{}\n".format(index + 1, value)
        bot.send_message(chat_id=message.chat.id, text=text, reply_markup=inline.send_test())
    else:
        bot.send_message(chat_id=message.chat.id,
                         text="Sizning savollaringiz berayotgan javoblaringiz bilan sonida farq qiladi")


@bot.callback_query_handler(func=lambda message: message.data == "i_approve")
def handle_approve(inline_query):
    message = inline_query.message
    chat = get_chat(message=message)
    data = chat.answer
    print('created at', chat.orders.values('created_at'))
    order_id, answers = data.split('#')
    order_items = OrderItem.objects.filter(order_id=order_id)
    correct_answer = []
    wrong_answer = []
    text = ''
    text_answer = ''
    print(order_items)
    order_created_at = chat.orders.values('created_at')
    for counter, order_item in enumerate(order_items):
        correct_variant = order_item.question.correct_variant()
        print('correct answer', correct_variant.id, counter)
        # print('order item', order_item)
        if answers[counter] == correct_variant.title:
            # is correct
            correct_answer.append(answers[counter])

        else:
            # is not correct
            wrong_answer.append(answers[counter])
    print('togri javobla', correct_answer)
    print('motogri javoblar', wrong_answer)
    for index, value in enumerate(answers):
        text_answer += "{}.{}\n".format(index + 1, value)
    text += "Test variant raqami: {}\n Test topshiruvch kodi: {}\n " \
            "Test topshiruvchi ismi: {}\n To'g'ri javoblar soni: {}\n" \
            "Noto'g'ri javoblar soni: {}\n -------------------------------------\n Javoblar \n {}" \
            "-------------------------------------\n Jami: 79 ball \n Test o'tqazilgan sana: {}".format(
        order_id, chat.chat_id, chat.username, len(correct_answer), len(wrong_answer), text_answer, order_created_at)
    bot.send_message(chat.chat_id, text=text, reply_markup=inline.show_wrong_answer())


@bot.callback_query_handler(func=lambda message: message.data == 'show_wrong_answer')
def handle_wrong_answers(inline_query):
    message = inline_query.message
    chat = get_chat(message=message)
    data = chat.answer
    order_id, answers = data.split('#')
    order_items = OrderItem.objects.filter(order_id=order_id)
    print(order_items.values('question__subject').first())
    answer = ''
    text = ''
    for counter, order_item in enumerate(order_items):
        correct_variant = order_item.question.correct_variant()
        if answers[counter] == correct_variant.title:
            cor_ans = answers[counter]
            cor_text = f'{counter + 1}{cor_ans} ✔\n'
            answer += cor_text
        else:
            # is not correct
            cor_ans = answers[counter]
            cor_text = f'{counter + 1}{cor_ans} ❌\n'
            answer += cor_text
    text += "{}".format(answer)
    bot.send_message(chat_id=chat.chat_id, text=text)


@bot.message_handler(func=lambda message: True, content_types=['text'])
def all_messages_handler(message):
    # Any test handler
    chat = get_chat(message)
    print('reastr', chat.step)
    if chat.step == chat.CHANGE_NAME_MENU:
        names = message.text.split(" ")
        if len(message.text) > 5 and len(names) == 2:
            chat.first_name = names[0]
            chat.last_name = names[1]
            chat.save()
            bot.send_message(chat.chat_id, messages.NAME_SUCCESSFUL_CHANGED)
        else:
            bot.send_message(chat.chat_id, messages.CHANGE_NAME_MSG)
    else:
        bot.send_message(chat_id=message.chat.id, text=messages.UNKNOWN_MSG)


@bot.callback_query_handler(func=lambda message: message.data == "go_back")
def go_back_handler(inline_query):
    # ◀ Orqaga
    message = inline_query.message
    chat = get_chat(message=message)
    if chat.step == chat.CHOOSE_TOPICS_MENU:
        subject_id = chat.selected_subjects.first().id
        subjected_test_subject_callback_query_handler(inline_query, subject_id)
    elif chat.step == chat.CHOOSE_SECTIONS_MENU:
        bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)
        subjected_take_tests_handler(message)
    elif chat.step in [chat.CHOOSE_SUBJECTS_MENU, chat.BLOCK_SUBJECTS_MENU]:
        bot.delete_message(chat_id=message.chat.id, message_id=message.message_id)
        take_test_handler(message)
    elif chat.step == chat.MENTOR_MENU:
        print(chat.step)
        bot.delete_message(chat_id=message.chat.id, message_id=message.id)
    else:
        print(chat.step)
        print(chat.step == chat.CHOOSE_SUBJECTS_MENU)


@bot.callback_query_handler(func=lambda message: re.search(r'groups_\d+$', message.data))
def group_menu(inline_query):
    # Guruh menusi
    message = inline_query.message
    chat = get_chat(message)
    chat.set_step(chat.SELECTED_GROUP)
    data_id = int(inline_query.data.split('groups_')[1])
    group = Group.objects.get(id=data_id)
    bot.delete_message(chat_id=message.chat.id, message_id=message.id - 1)
    bot.delete_message(chat_id=message.chat.id, message_id=message.id)
    bot.send_message(chat_id=message.chat.id, text="Guruh statistikasi", reply_markup=inline.statistic_group(group))
    bot.send_message(chat_id=message.chat.id, text="O'quvchilar nazorati", reply_markup=reply.students_supervision())


@bot.callback_query_handler(func=lambda message: re.search(r'group_\d+$', message.data))
def group_statistic(inline_query):
    # Guruh statistikkasini yuborish
    message = inline_query.message
    chat = get_chat(message)
    chat.set_step(chat.STATISTIC_VIEW)
    data_id = int(inline_query.data.split('group_')[1])
    group = Group.objects.get(id=data_id)
    bot.delete_message(chat_id=message.chat.id, message_id=message.id - 1)
    bot.delete_message(chat_id=message.chat.id, message_id=message.id)
    bot.send_photo(chat_id=message.chat.id, photo=group.statistic, reply_markup=reply.statistic())


@bot.callback_query_handler(func=lambda message: True)
def callback_query_handler(inline_query):
    message = inline_query.message
    bot.send_message(chat_id=message.chat.id, text=message.text)
