from apps.main.models import Chat
from apps.main.models import Order, OrderItem
__all__ = ['get_chat', 'get_full_name', 'get_order_detail', 'get_orders_text', 'get_transactions_text',
           'get_state_test_order_preview', 'get_subjected_test_order_preview', ]


def get_chat(message):
    chat_instance, is_created = Chat.objects.get_or_create(chat_id=message.chat.id)
    return chat_instance


def get_full_name(chat):
    username = chat.username
    first_name = chat.first_name if chat.first_name else ' '
    last_name = chat.last_name if chat.last_name else ' '
    if first_name or last_name:
        return "{} {}".format(first_name, last_name)
    return username


def get_orders_text(orders, chat):
    text = ""
    for index, order in enumerate(orders):
        numb = (chat.page - 1) * 12 + (index + 1)
        text += "<b>{}</b>. Order id: {}\n".format(numb, order.id)
        text += "      Date: {}\n".format(order.created_at.strftime("%d/%m/%Y"))
        text += "      Total price: {}\n".format(order.total_price)
        if order.test_type:
            text += "Test type: {}\n\n".format(order.get_test_type_display())
        if order.is_paid:
            text += "       Detail: /o{}\n".format(order.id)
        else:
            text += "       Detail: /pay{}\n".format(order.id)
    return text


def get_order_detail(order):
    text = "Order id: {}\n".format(order.id)
    text += "Date: {}\n".format(order.created_at.strftime("%d/%m/%Y"))
    text += "Total price: {}\n".format(order.total_price)
    if order.test_type:
        text += "Test type: {}\n\n".format(order.get_test_type_display())
    if order.is_paid:
        text += "Detail: /o{}\n".format(order.id)
    else:
        text += "Detail: /pay{}\n".format(order.id)
    return text


def get_transactions_text(transactions):
    text = ""
    if transactions.exists():
        for index, transaction in enumerate(transactions):
            text += "<b>{}</b>. ".format(index + 1)
            text += "Date: {}\n".format(transaction.created_at.strftime("%d/%m/%Y"))
            text += "Total price: {}\n".format(transaction.amount)
            text += "Is paid: {}\n\n".format("✅" if transaction.is_paid else "❌")
    else:
        text += "🤭 Uzr, sizda hozircha tranzaksiyalar mavjud emas!"
    return text


def get_subjected_test_order_preview(chat):
    text = "Mavzulashtirilgan: 2000 so'm \n"
    return text


def get_state_test_order_preview(chat):
    text = "Asosiy blok: 2000 so'm\n"
    text += "Qo'shimcha blok: 2000 so'm"
    return text
