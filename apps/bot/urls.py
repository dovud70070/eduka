from django.urls import path

from apps.bot.views import BotView, PaymentConfirmView

urlpatterns = [
    path('', BotView.as_view(), name='home'),
    path('payment_old/<int:pk>', PaymentConfirmView.as_view(), name='payment_confirm'),
]
