from django.http import JsonResponse
from rest_framework import permissions
from rest_framework.authentication import TokenAuthentication
from rest_framework.views import APIView

from apps.payment.Application import Application


class PaycomView(APIView):
    authentication_classes = (TokenAuthentication, )

    def post(self, request):
        print(request.data)
        app = Application(request=request)
        response = app.run()
        import json
        new_data = json.loads(response)
        return JsonResponse(data=new_data, safe=False)
