from django.db import models

from apps.main.models import Order


class PaycomTransaction(models.Model):
    paycom_transaction_id = models.CharField(max_length=100, null=True)
    paycom_time = models.BigIntegerField(null=True, blank=True)
    paycom_time_datetime = models.DateTimeField(null=True, blank=True)
    create_time = models.DateTimeField()
    amount = models.FloatField()
    state = models.IntegerField()
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    cancel_time = models.DateTimeField(null=True, blank=True)
    perform_time = models.DateTimeField(null=True, blank=True)
    reason = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return f'{self.order}s transactions'
