from django.contrib import admin
from .models import PaycomTransaction


class PaycomTransactionAdmin(admin.ModelAdmin):
    list_display = ['paycom_transaction_id', 'paycom_time', 'amount', 'state', 'order', ]


admin.site.register(PaycomTransaction, PaycomTransactionAdmin)
