from django.apps import AppConfig


class PaycomConfig(AppConfig):
    name = 'payment'
