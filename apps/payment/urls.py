from django.urls  import path

from apps.payment.views import PaycomView

urlpatterns = [
    path('payment/paycom/', PaycomView.as_view(), name='paycom'),
]