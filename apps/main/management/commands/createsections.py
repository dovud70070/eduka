from django.core.management.base import BaseCommand
from apps.main.models import Subject, Section


class Command(BaseCommand):
    help = 'Create sections'

    def handle(self, *args, **options):
        Section.objects.all().delete()
        id = 1
        for subject in Subject.objects.all():
            for index in range(1, 15):
                title = f"{subject.title} {index}-bo'lim"
                Section(id=id, subject=subject, title=title).save()
                id += 1
        data = f'Totally {Section.objects.count()} sections created.'
        self.stdout.write(self.style.SUCCESS(data))
