from django.core.management.base import BaseCommand
from apps.main.models import Question


class Command(BaseCommand):
    help = 'Clear all questions'

    def handle(self, *args, **options):
        all_items = Question.objects.all()
        total_items = all_items.count()
        all_items.delete()
        data = f'Totally {total_items} questions deleted.'
        self.stdout.write(self.style.SUCCESS(data))
