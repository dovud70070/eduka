from django.core.management.base import BaseCommand
from apps.main.models import Section


class Command(BaseCommand):
    help = 'Clear all sections'

    def handle(self, *args, **options):
        all_items = Section.objects.all()
        total_items = all_items.count()
        all_items.delete()
        data = f'Totally {total_items} sections deleted.'
        self.stdout.write(self.style.SUCCESS(data))
