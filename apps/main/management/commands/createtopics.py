from django.core.management.base import BaseCommand
from apps.main.models import Subject, Topic, Section, Class


class Command(BaseCommand):
    help = 'Create random topics'

    def handle(self, *args, **options):
        id = 1
        Topic.objects.all().delete()
        for _class in Class.objects.all():
            for subject in Subject.objects.all():
                for section in Section.objects.filter(subject=subject):
                    title = f"{subject.title} bo'yicha, {_class.title}, {section.title} {id}-mavzusi"
                    Topic(id=id, title=title, section=section, subject=subject, _class=_class).save()
                    id += 1

        data = f'Totally {id-1} topics created.'
        self.stdout.write(self.style.SUCCESS(data))
