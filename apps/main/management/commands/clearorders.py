from django.core.management.base import BaseCommand
from apps.main.models import Order


class Command(BaseCommand):
    help = 'Clear all orders'

    def handle(self, *args, **options):
        all_items = Order.objects.all()
        total_items = all_items.count()
        all_items.delete()
        data = f'Totally {total_items} orders deleted.'
        self.stdout.write(self.style.SUCCESS(data))
