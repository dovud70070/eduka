from django.core.management.base import BaseCommand
from apps.main.models import Topic


class Command(BaseCommand):
    help = 'Clear all topics'

    def handle(self, *args, **options):
        all_items = Topic.objects.all()
        total_items = all_items.count()
        all_items.delete()
        data = f'Totally {total_items} topics deleted.'
        self.stdout.write(self.style.SUCCESS(data))
