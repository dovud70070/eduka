from django.core.management.base import BaseCommand
from apps.main.models import Class


class Command(BaseCommand):
    help = 'Create random classes'

    def handle(self, *args, **options):
        total_created_classes = 0
        for index in range(5, 12):
            Class(title=f"{index}-sinf").save()
            total_created_classes += 1
        data = f'Totally {total_created_classes} classes created.'
        self.stdout.write(self.style.SUCCESS(data))
