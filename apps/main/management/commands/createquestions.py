import random

from django.core.management.base import BaseCommand
from apps.main.models import Question, Variant, Subject, Class, Topic, CustomUser, ExtraData


class Command(BaseCommand):
    help = 'Create random questions'

    def handle(self, *args, **options):
        Question.objects.all().delete()
        id = 1
        for _class in Class.objects.all():
            for subject in Subject.objects.all():
                for topic in Topic.objects.filter(subject=subject, _class=_class):
                    is_mandatory = random.randint(0, 1)
                    is_state_center_test = random.randint(0, 1)
                    level = random.randint(1, 10)
                    question = {"is_mandatory": is_mandatory, "is_state_center_test": is_state_center_test}
                    title = f"{subject.title}dan {id}-savol"
                    question.update({
                        "id": id,
                        "title": title,
                        "_class": _class,
                        "topic": topic,
                        "subject": subject,
                        "level": level,
                        "owner": CustomUser.objects.filter(is_active=True, type_of_user=ExtraData.ADMIN).first(),
                        "description": f"{_class.title} sinf {subject.title} bo'ycha {level}-darajali savol."
                    })
                    instance = Question(**question)
                    instance.save()

                    stage = random.randint(0, 3)
                    for index, variant in enumerate(Variant.variants):
                        variant_desc = "<p style='text-align: center;'><math class='wrs_chemistry' xmlns='http://www.w3.org/1998/Math/MathML'><mn>2</mn><msub><mi mathvariant='normal'>H</mi><mn>2</mn></msub><mfenced><mi mathvariant='normal'>g</mi></mfenced><mo>+</mo><msub><mi mathvariant='normal'>O</mi><mn>2</mn></msub><mfenced><mi mathvariant='normal'>g</mi></mfenced><mo>⇌</mo><mn>2</mn><msub><mi mathvariant='normal'>H</mi><mn>2</mn></msub><mi mathvariant='normal'>O</mi><mfenced><mi mathvariant='normal'>l</mi></mfenced></math></p>"
                        is_correct = False
                        if index == stage:
                            is_correct = True
                        Variant(title=variant[0], description=variant_desc, question=instance,
                                is_correct=is_correct).save()
                    id += 1
        data = f'Totally {id-1} questions created.'
        self.stdout.write(self.style.SUCCESS(data))
