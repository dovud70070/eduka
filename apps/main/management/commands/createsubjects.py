from django.core.management.base import BaseCommand
from apps.main.models import Class, Subject


class Command(BaseCommand):
    help = 'Create subjects'

    def get_subjects(self):
        return [
            {
                "id": 1,
                "title": "Matematika",
                "is_thematic": True,
                "is_mandatory_block_subject": True,
            },
            {
                "id": 2,
                "title": "Tarix",
                "is_thematic": False,
                "is_mandatory_block_subject": True,
            },
            {
                "id": 3,
                "title": "Ona tili",
                "is_thematic": False,
                "is_mandatory_block_subject": True,
            },
            {
                "id": 4,
                "title": "Biologiya",
                "is_thematic": False,
                "is_mandatory_block_subject": False,
            },
            {
                "id": 5,
                "title": "Kimyo",
                "is_thematic": False,
                "is_mandatory_block_subject": False,
            },
            {
                "id": 6,
                "title": "Fizika",
                "is_thematic": True,
                "is_mandatory_block_subject": False,
            },
            {
                "id": 7,
                "title": "Ingliz tili",
                "is_thematic": True,
                "is_mandatory_block_subject": False,
            },
            {
                "id": 8,
                "title": "Chet tili",
                "is_thematic": True,
                "is_mandatory_block_subject": False,
            }
        ]

    def handle(self, *args, **options):
        Subject.objects.all().delete()
        for subject in self.get_subjects():
            Subject(**subject).save()
        data = f'Totally {len(self.get_subjects())} subjects created.'
        self.stdout.write(self.style.SUCCESS(data))
