from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.utils.translation import gettext_lazy as _

from apps.main.models import *
from apps.main.models import Solution, PaymentHistory
from apps.payment.models import PaycomTransaction


class ChatAdmin(admin.ModelAdmin):
    pass


class SubjectAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'is_thematic', )
    list_filter = ('is_thematic', 'is_mandatory_block_subject', )


class ClassAdmin(admin.ModelAdmin):
    list_display = ('id', 'title',)


class SectionAdmin(admin.ModelAdmin):
    list_display = ('title',)
    list_filter = ('subject', )


class TopicAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'section', 'subject', '_class',)


class VariantInlineAdmin(admin.TabularInline):
    model = Variant
    fields = ('title', 'description', 'is_correct',)


class SolutionInlineAdmin(admin.StackedInline):
    model = Solution
    fields = ('text_solution', )
    extra = 4


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('id','title', 'topic',)
    inlines = (VariantInlineAdmin, SolutionInlineAdmin)
    list_filter = ('subject', 'level', '_class')


class OrderItemInlineAdmin(admin.TabularInline):
    model = OrderItem
    fields = ('subject', 'price', )


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'purchaser', 'total_price', 'created_at', 'created_at', 'file',)
    inlines = (OrderItemInlineAdmin,)


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('id', 'owner', 'amount', 'payment_method', 'is_paid',)
    list_filter = ('is_paid',)


class OrderItemAdmin(admin.ModelAdmin):
    list_display = ['id', 'price']


class GroupAdmin(admin.ModelAdmin):
    list_display = ('title',)


class CustomUserAdmin(auth_admin.UserAdmin):
    list_display = ['id', 'username', 'balance']
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'phone', 'balance', 'type_of_user')}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2'),
        }),
    )


class BalanceAdmin(admin.ModelAdmin):
    list_display = ['id', 'balance']


class WithdrawAdmin(admin.ModelAdmin):
    list_display = ['id', ]


class QuestionLevelInlineAdmin(admin.StackedInline):
    model = QuestionLevel
    fields = ['level', 'setting', ]


class QuestionSettingsAdmin(admin.ModelAdmin):
    list_display = ['id', 'subject', 'number']
    inlines = [QuestionLevelInlineAdmin, ]


class SolutionAdmin(admin.ModelAdmin):
    list_display = ['text_solution', ]


class PaymentHistoryAdmin(admin.ModelAdmin):
    list_display = ['amount', 'chat', 'order', 'user', 'created_at',]
    # readonly_fields = ['amount', 'chat', 'order', 'user', 'created_at',]

    # def has_delete_permission(self, request, obj=None):
    #     return False


class QuestionLevelAdmin(admin.ModelAdmin):
    list_display = ['id', 'level']


admin.site.register(Chat, ChatAdmin)
admin.site.register(Subject, SubjectAdmin)
admin.site.register(Class, ClassAdmin)
admin.site.register(Section, SectionAdmin)
admin.site.register(Topic, TopicAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(OrderItem, OrderItemAdmin)
admin.site.register(Group, GroupAdmin)
admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Withdraw, WithdrawAdmin)
admin.site.register(QuestionSetting, QuestionSettingsAdmin)
admin.site.register(Solution, SolutionAdmin)
admin.site.register(PaymentHistory, PaymentHistoryAdmin)
admin.site.register(QuestionLevel, QuestionLevelAdmin)
