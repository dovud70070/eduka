from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.mail import send_mail
from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.utils.text import gettext_lazy as _
from django.conf import settings
from apps.main.managers import UserManager

__all__ = ['Chat', 'Subject', 'Class', 'Section', 'Topic', 'Question', 'Order', 'OrderItem', 'Variant',
           'Transaction', 'Group', 'CustomUser', 'ExtraData', 'Withdraw', 'QuestionSetting', 'QuestionLevel',
           'Solution', 'PaymentHistory', ]


class ExtraData:
    STATE_CENTER_TEST = 'state_center_test'
    SUBJECTED_TEST = 'subjected_test'

    TEST_FORMATS = (
        (STATE_CENTER_TEST, "DTM test"),
        (SUBJECTED_TEST, "Subjected test"),
    )
    ADMIN = 'admin'
    ENTRANT = 'entrant'
    MENTOR = 'mentor'
    MODERATOR = 'moderator'
    USER_TYPES = (
        (ENTRANT, "Entrant"),
        (MENTOR, "Mentor"),
        (MODERATOR, 'Moderator'),
        (ADMIN, 'Admin'),
    )
    WELCOME_MENU = "welcome"
    USER_TYPE_MENU = "user_type"
    ENTRANT_MENU = "entrant"
    TEST_FORMAT_MENU = "test_format"
    BLOCK_SUBJECTS_MENU = "block_subjects"
    BLOCK_SUBJECTS_ORDER_CONFIRM_MENU = "block_subjects_order_confirm"
    BLOCK_SUBJECTS_ORDER_NOT_ENOUGH_MONEY_MENU = "block_subjects_order_not_enough_money"
    BLOCK_SUBJECTS_ORDER_FILE_MENU = "block_subjects_order_file"
    CHOOSE_SUBJECTS_MENU = "choose_subjects"
    CHOOSE_SECTIONS_MENU = "choose_sections"
    CHOOSE_CLASS_MENU = "choose_class"
    CHOOSE_TOPICS_MENU = "choose_topics"
    SUBJECT_TEST_ORDER_CONFIRM_MENU = "subjected_test_order_confirm"
    NOT_SUBJECT_TEST_ORDER_CONFIRM_MENU = "not_subjected_test_order_confirm"
    MY_ORDERS_MENU = "my_orders"
    FILLING_BALANCE_MENU = "filling_balance"
    MY_BALANCE_MENU = "my_balance"
    CHOOSE_PAYMENT_METHOD_MENU = "choose_payment_method"
    MY_SETTINGS_MENU = "my_settings"
    CHANGE_PHONE_MENU = "change_phone"
    CHANGE_NAME_MENU = "change_name"
    MY_RESULTS_MENU = "my_results"
    MENTOR_MENU = "mentor"
    ADD_GROUP = "add_group"
    SELECTED_GROUP = "selected_group"
    STATISTIC_VIEW = "statistic_view"
    STUDEND_CONTROL = "student_control"
    CHECK_RESULT = 'check_result'

    STEPS = (
        (WELCOME_MENU, "Welcome menu"),
        (USER_TYPE_MENU, "User type menu"),
        (ENTRANT_MENU, "Entrant menu"),
        (TEST_FORMAT_MENU, "Test format menu"),
        (BLOCK_SUBJECTS_MENU, "Block subjects menu"),
        (BLOCK_SUBJECTS_ORDER_CONFIRM_MENU, "Block subjects order confirm menu"),
        (BLOCK_SUBJECTS_ORDER_NOT_ENOUGH_MONEY_MENU, "Block subjects order not enough money menu"),
        (BLOCK_SUBJECTS_ORDER_FILE_MENU, "Block subjects order file menu"),
        (CHOOSE_SUBJECTS_MENU, "Choose subjects"),
        (CHOOSE_SECTIONS_MENU, "Choose sections"),
        (CHOOSE_CLASS_MENU, "Choose class"),
        (CHOOSE_TOPICS_MENU, "Choose topics"),
        (MY_ORDERS_MENU, "My orders"),
        (FILLING_BALANCE_MENU, "Filling balance"),
        (MY_BALANCE_MENU, "My balance"),
        (CHOOSE_PAYMENT_METHOD_MENU, "Choose payment_old method"),
        (MY_SETTINGS_MENU, "My settings"),
        (CHANGE_PHONE_MENU, "Change phone"),
        (CHANGE_NAME_MENU, "Change name"),
        (MY_RESULTS_MENU, "My results"),
        (MENTOR_MENU, "Mentor menu"),
        (ADD_GROUP, "Add group"),
        (SELECTED_GROUP, "Selected group"),
        (STATISTIC_VIEW, "Statistic view"),
        (STUDEND_CONTROL, "Student Control"),
        (CHECK_RESULT, "Check result"),
    )


class CustomUser(AbstractUser):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username and password are required. Other fields are optional.
    """
    username_validator = UnicodeUsernameValidator()
    username = models.CharField(
        _('username'),
        max_length=150,
        unique=True,
        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
        validators=[username_validator],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
    )
    first_name = models.CharField(_('first name'), max_length=150, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    phone = models.CharField(
        max_length=60,
        null=True,
        blank=True,
        unique=True,
        error_messages={
            'unique': _("A user with that phone already exists.")
        },
    )
    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(
        _('staff status'),
        default=False,
        help_text=_('Designates whether the user can log into this admin site.'),
    )
    is_active = models.BooleanField(
        _('active'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    type_of_user = models.CharField(choices=ExtraData.USER_TYPES, null=True, max_length=150)

    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    balance = models.FloatField(default=0)
    objects = UserManager()

    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_phone(self):
        return str(self.phone).replace("+998", "")


class Chat(models.Model, ExtraData):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, null=True, blank=True, on_delete=models.SET_NULL)
    user_type = models.CharField(max_length=250, choices=ExtraData.USER_TYPES, default=ExtraData.ENTRANT)
    chat_id = models.IntegerField(null=True)
    first_name = models.CharField(null=True, max_length=250, blank=True)
    last_name = models.CharField(null=True, max_length=250, blank=True)
    username = models.CharField(null=True, max_length=250, blank=True)
    phone = models.CharField(null=True, max_length=250, blank=True, unique=True)
    is_active = models.BooleanField(default=True)
    selected_test_format = models.CharField(max_length=25, choices=ExtraData.TEST_FORMATS, null=True, blank=True)
    selected_subjects = models.ManyToManyField('Subject', blank=True)
    selected_topics = models.ManyToManyField('Topic', blank=True)
    selected_classes = models.ManyToManyField('Class', blank=True)
    groups = models.ManyToManyField('Group', blank=True, related_name="chats")
    would_like_mandatory_tests = models.BooleanField(null=True, blank=True)
    step = models.CharField(max_length=255, choices=ExtraData.STEPS, null=True, blank=True)
    balance = models.FloatField(default=0)
    page = models.IntegerField(default=0)
    payment_amount = models.IntegerField(default=0)
    answer = models.CharField(max_length=250, blank=True, null=True)

    def __str__(self):
        if self.first_name or self.last_name:
            return "{} {}".format(self.first_name, self.last_name)
        elif self.username:
            return self.username
        elif self.phone:
            return self.phone
        return '<Chat {}>'.format(self.chat_id)

    def clear(self):
        self.selected_test_format = ''
        self.selected_subjects.clear()
        self.selected_classes.clear()
        self.selected_topics.clear()
        self.would_like_mandatory_tests = False
        self.save()

    def set_step(self, step):
        self.step = step
        self.save()

    def make_payment_for(self, order):
        self.balance -= float(order.total_price)
        self.save()
        PaymentHistory(chat=self, amount=order.total_price, order=order).save()
        order.is_paid = True
        order.save()


class Subject(models.Model):
    title = models.CharField(max_length=250)
    is_thematic = models.BooleanField(default=False)
    is_mandatory_block_subject = models.BooleanField(default=False)

    def __str__(self):
        return self.title


class Class(models.Model):
    title = models.CharField(max_length=250)

    class Meta:
        verbose_name = 'Class'
        verbose_name_plural = 'Classes'

    def __str__(self):
        return self.title


class Group(models.Model):
    title = models.CharField(max_length=25)
    students = models.ManyToManyField(Chat, blank=True, related_name='group')
    statistic = models.FileField(null=True)

    def __str__(self):
        return self.title


class Section(models.Model):
    title = models.CharField(max_length=250)
    subject = models.ForeignKey(Subject, null=True, on_delete=models.CASCADE, related_name="sections")

    def __str__(self):
        return self.title


class Topic(models.Model):
    title = models.CharField(max_length=250)
    section = models.ForeignKey(Section, null=True, on_delete=models.CASCADE, related_name="topics", blank=True)
    subject = models.ForeignKey(Subject, null=True, on_delete=models.CASCADE, related_name="topics", blank=True)
    _class = models.ForeignKey(Class, null=True, on_delete=models.CASCADE, related_name="topics", blank=True)

    def __str__(self):
        return self.title


class Question(models.Model):
    title = models.CharField(max_length=200, null=True, blank=True)
    description = models.TextField(null=True)
    topic = models.ForeignKey(Topic, null=True, on_delete=models.CASCADE, related_name="questions")
    is_mandatory = models.BooleanField(default=False)
    is_state_center_test = models.BooleanField(default=False)
    is_themed = models.BooleanField(default=False)
    _class = models.ForeignKey(Class, null=True, on_delete=models.CASCADE, related_name="questions")
    subject = models.ForeignKey(Subject, null=True, on_delete=models.CASCADE, related_name="questions")
    level = models.PositiveIntegerField(null=True)
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=True)
    enabled = models.BooleanField(default=True)
    text_solution = models.TextField(null=True)

    def __str__(self):
        return self.description

    def correct_variant(self):
        return self.variants.filter(is_correct=True).first()


class Variant(models.Model):
    A = "A"
    B = "B"
    C = "C"
    D = "D"
    variants = (
        (A, "A"),
        (B, "B"),
        (C, "C"),
        (D, "D"),
    )
    title = models.CharField(max_length=250, choices=variants, default=A)
    description = models.TextField(null=True, blank=True)
    is_correct = models.BooleanField(default=False)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name="variants", null=True, blank=True)

    def __str__(self):
        return f'{self.title}{self.id}'

    class Meta:
        ordering = ('title', )


class Order(models.Model):
    STATE_AVAILABLE = 0
    STATE_WAITING_PAY = 1
    STATE_PAY_ACCEPTED = 2
    STATE_CANCELLED = 3
    STATE_DELIVERED = 4
    PAYME_STATUS = (
        (STATE_AVAILABLE, "ORDER TUSHDI"),
        (STATE_WAITING_PAY, "TO'LOVNI KUTYAPTI"),
        (STATE_PAY_ACCEPTED, "TO'L0V QABUL QILINDI"),
        (STATE_CANCELLED, "QOLDIRILDI"),
        (STATE_DELIVERED, "YETKAZILDI"),
    )
    state = models.IntegerField(choices=PAYME_STATUS, default=STATE_AVAILABLE)
    purchaser = models.ForeignKey(Chat, on_delete=models.CASCADE, related_name="orders", null=True)
    total_price = models.FloatField(default=0)
    created_at = models.DateTimeField(auto_created=True, null=True)
    file = models.FileField(null=True)
    is_paid = models.BooleanField(default=False)
    test_type = models.CharField(choices=ExtraData.TEST_FORMATS, max_length=255, null=True)

    def set_paid(self):
        self.is_paid = True
        self.purchaser.balance -= self.total_price
        self.purchaser.save()
        self.save()

    def __str__(self):
        return "<Order - {}>".format(self.id)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name="items")
    price = models.DecimalField(max_digits=9, decimal_places=2, default=0)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name="ordered_subjects", null=True)


class OrderItemQuestion(models.Model):
    order_item = models.ForeignKey(OrderItem, on_delete=models.CASCADE, related_name="order_items")
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name="ordered_items")
    is_solved_correctly = models.BooleanField(default=False)


class Withdraw(models.Model):
    status = models.BooleanField(default=False)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='withdraw', null=True)
    summa = models.FloatField(default=0)
    created_at = models.DateTimeField(auto_now=True, null=True)


class Transaction(models.Model):
    PAYME = 'payme'
    CLICK = 'click'
    METHODS = (
        (PAYME, "Payme",),
        (CLICK, "Click",)
    )
    amount = models.FloatField()
    owner = models.ForeignKey(Chat, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    payment_method = models.CharField(choices=METHODS, max_length=255)
    is_paid = models.BooleanField(default=False)


class QuestionSetting(models.Model):
    THEMED_TEST = 'themed_test'
    TEST_FORMATS = ExtraData.TEST_FORMATS + ((THEMED_TEST, 'Themed test'), )
    number = models.IntegerField(default=1)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='question_settings')
    test_type = models.CharField(choices=TEST_FORMATS, max_length=255, null=True)

    def __str__(self):
        return "{} - {}".format(self.subject.title, self.number)


class QuestionLevel(models.Model):
    level = models.IntegerField(default=1)
    setting = models.ForeignKey(QuestionSetting, on_delete=models.CASCADE, related_name='question_levels', null=True)
    topic = models.ManyToManyField(Topic, blank=True)

    def __str__(self):
        return f'id - {self.id}, level - {self.level}'


class Solution(models.Model):
    text_solution = RichTextUploadingField(null=True, blank=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='solution')


class PaymentHistory(models.Model):
    amount = models.FloatField()
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE, related_name='payment_histories', null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='payment_histories',
                             null=True)
    order = models.ForeignKey(Order, on_delete=models.CASCADE, related_name='payment_histories', null=True)
    created_at = models.DateField(auto_now_add=True, null=True)
