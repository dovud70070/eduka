# Generated by Django 3.1.4 on 2021-01-06 11:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0007_auto_20210105_2021'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='title',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
