var mjAPI = require("mathjax-node");
var argv = require("yargs").argv;

mjAPI.config({
    MathJax: {},
});
mjAPI.start();
mjAPI.typeset({
    math: argv._[0],
    format: "MathML",
    svg: true,
}, function (data) {
    if (!data.errors) {console.log(data.svg)}
})
