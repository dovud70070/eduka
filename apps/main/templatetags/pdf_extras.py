import re
from django import template

register = template.Library()


def mml2svg(mathml):
    import subprocess
    proc = subprocess.Popen(['node', 'math2svg.js', mathml], stdout=subprocess.PIPE)
    output = proc.stdout.read().decode('utf-8')
    return output


def my_replace(match):
    match = match.group()
    return mml2svg(match)


def mml_replace_with_svg(html_with_mathml):
    result = re.sub(r"<math (.*)<\/math>", my_replace, html_with_mathml)
    return result


@register.filter
def replace_with_svg(data):
    return mml_replace_with_svg(data)
