from django.urls import path
from rest_framework import routers

from apps.restful import views as rest_full_views
from .views import TokenGenerateView, TokenRefreshView
from apps.restful.views import TransactionListView, OrderDownloadView, ChangePasswordView, \
    StudentOrderDownload,SubjectViewSet, SectionViewSet, QuestionSettingsViewSet

router = routers.SimpleRouter()
router.register('questions', rest_full_views.QuestionViewSet)
router.register('topics', rest_full_views.TopicViewSet)
router.register('classes', rest_full_views.ClassViewSet)
router.register('variants', rest_full_views.VariantViewSet)
router.register('orders', rest_full_views.OrderViewSet)
router.register('order-items', rest_full_views.OrderItemViewSet)
router.register('student', rest_full_views.StudentViewSet)
router.register('teacher', rest_full_views.TeacherViewSet)
router.register('group', rest_full_views.GroupViewSet)
router.register('moderator', rest_full_views.ModeratorViewSet)
router.register('withdraw', rest_full_views.WithdrawViewSet)
router.register('subjects', SubjectViewSet)
router.register('sections', SectionViewSet)
router.register('question-settings', QuestionSettingsViewSet)
router.register('question-level', rest_full_views.QuestionLevelTopicViewSet)


urlpatterns = router.urls + [
    path('transactions/', TransactionListView.as_view()),
    path('orders/<int:pk>/xls/', OrderDownloadView.as_view(), name='order_download_as_xls'),
    path('studend-orders-download/<int:pk>/', StudentOrderDownload.as_view()),
    path('change-password/', ChangePasswordView.as_view(), name='change-password'),
    path('token/', TokenGenerateView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_obtain_pair'),
    path('user-updata/', rest_full_views.UserView.as_view()),
    path('payment-history', rest_full_views.PaymentHistoryViewSet.as_view()),
]
