from rest_framework.permissions import BasePermission
from apps.main.models import ExtraData


class IsAdminIsModerator(BasePermission):
    """Allow access to owners"""

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated) and request.user.type_of_user in \
               [ExtraData.ADMIN, ExtraData.MODERATOR]

    def has_object_permission(self, request, view, obj):
        return bool(request.user and request.user.is_authenticated) and request.user.type_of_user in \
               [ExtraData.ADMIN, ExtraData.MODERATOR]


class IsAdminIsTeacher(BasePermission):

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_authenticated) and request.user.type_of_user in \
               [ExtraData.ADMIN, ExtraData.MENTOR]

    def has_object_permission(self, request, view, obj):
        return bool(request.user and request.user.is_authenticated) and request.user.type_of_user in \
               [ExtraData.ADMIN, ExtraData.MENTOR]
