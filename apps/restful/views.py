import os
from django.http import JsonResponse
from clickuz.click_authorization import click_authorization
from clickuz.serializer import ClickUzSerializer
from clickuz.status import AUTHORIZATION_FAIL, A_LACK_OF_MONEY_CODE, INVALID_AMOUNT, INVALID_ACTION, \
    TRANSACTION_NOT_FOUND
from django.conf import settings
from django.db.models import Q
from django.http import Http404, HttpResponse

import xlwt as xlwt
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.decorators import action
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.views import (TokenObtainPairView as JWTTokenObtainPairView,
                                            TokenRefreshView as JWTTokenRefreshView,
                                            )
from rest_framework import generics, response, permissions, status as rest_status

from .permissions import IsAdminIsModerator
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from . import serializers as restful_serializers
from apps.restful import filter_params
from apps.restful.serializers import UserSerializerWithToken, TokenSerializer

from apps.main.models import *

from clickuz.views import ClickUzMerchantAPIView, PREPARE, COMPLETE, AUTHORIZATION_FAIL_CODE, A_LACK_OF_MONEY

from ..main.models import PaymentHistory

from clickuz import ClickUz


class OrderCheckAndPayment(ClickUz):
    def check_order(self, order_id: str, amount: str):
        try:
            order = Transaction.objects.get(id=order_id)
            if order:
                return self.ORDER_FOUND
        except Exception:
            return self.ORDER_NOT_FOUND

    def successfully_payment(self, order_id: str, transaction: object):
        main_transaction = Transaction.objects.get(id=order_id)
        main_transaction.is_paid = True
        main_transaction.save()


class TestView(ClickUzMerchantAPIView):
    VALIDATE_CLASS = OrderCheckAndPayment

    def post(self, request):
        serializer = ClickUzSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        METHODS = {
            PREPARE: self.prepare,
            COMPLETE: self.complete
        }

        merchant_trans_id = serializer.validated_data['merchant_trans_id']
        amount = serializer.validated_data['amount']
        action = serializer.validated_data['action']

        if click_authorization(**serializer.validated_data) is False:
            return response.Response({
                "error": AUTHORIZATION_FAIL_CODE,
                "error_note": AUTHORIZATION_FAIL
            })

        assert self.VALIDATE_CLASS != None
        check_order = self.VALIDATE_CLASS().check_order(order_id=merchant_trans_id, amount=amount)
        if check_order is True:
            result = METHODS[action](**serializer.validated_data, response_data=serializer.validated_data)
            return response.Response(result)
        return response.Response({"error": check_order, "error_note": "Someething"})

    def prepare(self, click_trans_id: str, merchant_trans_id: str, amount: str, sign_string: str, sign_time: str,
                response_data: dict,
                *args, **kwargs) -> dict:
        """

        :param click_trans_id:
        :param merchant_trans_id:
        :param amount:
        :param sign_string:
        :param response_data:
        :param args:
        :return:
        """
        transaction = Transaction.objects.create(
            click_trans_id=click_trans_id,
            merchant_trans_id=merchant_trans_id,
            amount=amount,
            action=PREPARE,
            sign_string=sign_string,
            sign_datetime=sign_time,
        )

        response_data.update(merchant_prepare_id=transaction.id)
        return response_data

    def complete(self, click_trans_id, amount, error, merchant_prepare_id,
                 response_data, action, *args, **kwargs):
        """

        :param click_trans_id:
        :param merchant_trans_id:
        :param amount:
        :param sign_string:
        :param error:
        :param merchant_prepare_id:
        :param response_data:
        :param action:
        :param args:
        :return:
        """
        try:
            transaction = Transaction.objects.get(pk=merchant_prepare_id)

            if error == A_LACK_OF_MONEY:
                response_data.update(error=A_LACK_OF_MONEY_CODE, error_note="Something")
                transaction.action = A_LACK_OF_MONEY
                transaction.status = Transaction.CANCELED
                transaction.save()
                return response_data

            if transaction.action == A_LACK_OF_MONEY:
                response_data.update(error=A_LACK_OF_MONEY_CODE, error_note="Something")
                return response_data

            if transaction.amount != amount:
                response_data.update(error=INVALID_AMOUNT, error_note="Something")
                return response_data

            if transaction.action == action:
                response_data.update(error=INVALID_ACTION, error_note="Something")
                return response_data

            transaction.action = action
            transaction.status = Transaction.FINISHED
            transaction.save()
            response_data.update(merchant_confirm_id=transaction.id)
            self.VALIDATE_CLASS().successfully_payment(transaction.merchant_trans_id, transaction)
            return response_data
        except Transaction.DoesNotExist:
            response_data.update(error=TRANSACTION_NOT_FOUND, error_note="Something")
            return response_data



class OrderCheckoutAndPPayment(ClickUz):
    def check_order(self, order_id: str, amount: str):
        try:
            order = Transaction.objects.get(id=order_id)
            if order:
                return self.ORDER_FOUND
        except Exception:
            return self.ORDER_NOT_FOUND

    def successfully_payment(self, order_id: str, transaction: object):
        print(order_id)
        order = Order.objects.get(id=order_id)
        order.click_state = Order.STATE_PAY_ACCEPTED
        order.save()


class PaymentConfirmView(APIView):
    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        print(pk)
        transaction = Transaction.objects.get(pk=pk)
        transaction.owner.balance += transaction.amount
        transaction.is_paid = True
        transaction.owner.save()
        transaction.save()
        return Response(data="Hello transaction")


class SubjectViewSet(ModelViewSet):
    queryset = Subject.objects.all().order_by('-id')
    serializer_class = restful_serializers.SubjectSerializer
    permission_classes = [IsAdminIsModerator, ]


class ClassViewSet(ModelViewSet):
    queryset = Class.objects.all().order_by('-id')
    serializer_class = restful_serializers.ClassSerializer
    permission_classes = [IsAdminIsModerator, ]


class SectionViewSet(ModelViewSet):
    queryset = Section.objects.all().order_by('-id')
    serializer_class = restful_serializers.SectionSerializer
    permission_classes = [IsAdminIsModerator, ]


class TopicViewSet(ModelViewSet):
    queryset = Topic.objects.all().order_by('-id')
    serializer_class = restful_serializers.TopicSerializer
    query_params = filter_params.get_topics_params()
    permission_classes = [IsAdminIsModerator, ]

    def get_queryset(self):
        queryset = super().get_queryset()
        queryparams = self.request.query_params
        if queryparams.get('subject_id', None):
            queryset = queryset.filter(subject_id=queryparams.get('subject_id'))
        if queryparams.get('_class_id', None):
            queryset = queryset.filter(_class_id=queryparams.get("_class_id"))
        return queryset

    @swagger_auto_schema(manual_parameters=query_params)
    def list(self, request, *args, **kwargs):
        return super(TopicViewSet, self).list(kwargs)


class QuestionViewSet(ModelViewSet):
    queryset = Question.objects.order_by('-id')
    query_params = filter_params.get_question_params()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_serializer_class(self):
        serializer = restful_serializers.QuestionSerializer
        if self.action == 'partial_update':
            serializer = restful_serializers.QuestionUpdateSerializer
        return serializer

    def get_queryset(self):
        queryset = super().get_queryset()
        queryparams = self.request.query_params
        query = queryparams.get('query', None)
        qargs = {}
        if self.request.user.is_authenticated and self.request.user.type_of_user == ExtraData.MODERATOR:
            qargs.update({
                'owner': self.request.user
            })

        title = queryparams.get('title', None)
        if title:
            qargs.update({'title__icontains': title})

        subject_id_list = queryparams.get('subject_id_list', None)
        if subject_id_list:
            qargs.update({'subject_id__in': subject_id_list.split(',')})

        topic = queryparams.get('topic', None)
        if topic:
            qargs.update({'topic__title__icontains': topic})

        is_mandatory = queryparams.get('is_mandatory', None)
        if is_mandatory:
            qargs.update({'is_mandatory': is_mandatory})

        is_state_center_test = queryparams.get('is_state_center_test', None)
        if is_state_center_test:
            qargs.update({'is_state_center_test': is_state_center_test})

        _class_id_list = queryparams.get('_class_id_list', None)
        if _class_id_list:
            qargs.update({'_class_id__in': _class_id_list.split(',')})

        owner_list = queryparams.get('owner_list', None)
        if owner_list:
            qargs.update({'owner__username__in': owner_list.split(',')})

        from_level = queryparams.get('from_level', None)
        if from_level:
            qargs.update({'level__gte': from_level})

        to_level = queryparams.get('to_level', None)
        if to_level:
            qargs.update({'level__lte': to_level})

        enabled = queryparams.get('enabled', None)
        if enabled:
            qargs.update({'enabled': enabled})

        return queryset.filter(**qargs)

    @swagger_auto_schema(manual_parameters=query_params)
    def list(self, request, *args, **kwargs):
        return super(QuestionViewSet, self).list(kwargs)


class QuestionLevelTopicViewSet(ModelViewSet):
    queryset = QuestionLevel.objects.all()
    serializer_class = restful_serializers.QuestionLevelTopicSerializer

    # def get_serializer_class(self):


class VariantViewSet(ModelViewSet):
    queryset = Variant.objects.all()
    serializer_class = restful_serializers.VariantSerializer
    permission_classes = [IsAdminIsModerator, ]


class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all().order_by('-id')
    serializer_class = restful_serializers.OrderSerializer
    permission_classes = [IsAdminIsModerator, ]


class OrderDownloadView(generics.RetrieveAPIView):

    def get_object(self, *args, **kwargs):
        pk = self.kwargs.get('pk')
        try:
            return Order.objects.get(pk=pk)
        except Order.DoesNotExist:
            raise Http404

    def get(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        order = self.get_object(pk)

        wb = xlwt.Workbook()
        ws = wb.add_sheet('Order')
        lefted_style = xlwt.easyxf('align: wrap on, vert center, horiz left;')
        centered_style = xlwt.easyxf('align: wrap on, vert center, horiz center;')

        zero_col = ws.col(0)
        zero_col.width = 1040

        fir_col = ws.col(1)
        fir_col.width = 1000

        sec_col = ws.col(2)
        sec_col.width = 12040

        fifth_col = ws.col(5)
        fifth_col.width = 3040

        invoice_style = xlwt.easyxf('font: bold on, height 280;')
        ws.write_merge(0, 0, 2, 6, "Инвойс", style=invoice_style)

        ws.write(2, 2, "Буюртма санаси")
        ws.write_merge(2, 2, 3, 6, order.created_at.strftime("%d/%m/%Y"), style=lefted_style)
        ws.write(3, 2, "Буюртма рақами")
        ws.write_merge(3, 3, 3, 6, order.id, style=lefted_style)
        ws.write(4, 2, "Purchaser first name")
        ws.write_merge(4, 4, 3, 6, order.purchaser.first_name if order.purchaser else order.purchaser_first_name,
                       style=lefted_style)
        ws.write(5, 2, "purchaser last name")
        ws.write_merge(5, 5, 3, 6, order.purchaser.last_name if order.purchaser else order.purchaser_last_name)
        ws.write(6, 2, "Test Type")
        ws.write_merge(6, 6, 3, 6, order.test_type, style=lefted_style)

        green_background_style = xlwt.XFStyle()
        bold_font = xlwt.Font()
        bold_font.bold = True
        green_background_style.font = bold_font
        pattern = xlwt.Pattern()
        pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        pattern.pattern_fore_colour = xlwt.Style.colour_map['green']
        green_background_style.pattern = pattern
        green_background_style.font.colour_index = xlwt.Style.colour_map['white']

        current_row = 10

        currency_format = xlwt.XFStyle()
        currency_format.num_format_str = '#,##0'

        row_starts_at = current_row
        row_ends_at = current_row

        current_row += 2

        centered_green_style = xlwt.easyxf('align: wrap on, vert center, horiz center; pattern: pattern solid, '
                                           'fore_colour green; font: color white;')
        ws.write(current_row, 0, "", style=green_background_style)
        ws.write(current_row, 1, "", style=green_background_style)
        ws.write(current_row, 2, "Жами", style=green_background_style)
        ws.write(current_row, 3, "", style=green_background_style)
        ws.write(current_row, 4, xlwt.Formula("SUM(e{}:e{})".format(row_starts_at, row_ends_at)),
                 style=centered_green_style)
        ws.write(current_row, 5, "", style=green_background_style)
        green_style_currency_format = xlwt.easyxf('pattern: pattern solid, fore_colour green; font: color white;')
        green_style_currency_format.num_format_str = '#,##0'
        ws.write(current_row, 6, xlwt.Formula("SUM(g{}:g{})".format(row_starts_at, row_ends_at)),
                 style=green_style_currency_format)

        res = HttpResponse(content_type='application/ms-excel')

        res['Content-Disposition'] = 'attachment; filename="report.xls"'

        wb.save(res)
        return res


class OrderItemViewSet(ModelViewSet):
    queryset = OrderItem.objects.all()
    serializer_class = restful_serializers.OrderItemSerializer
    permission_classes = [permissions.IsAdminUser, ]


class TransactionListView(APIView):
    def get(self, request):
        transaction = Transaction.objects.all()
        serializer = restful_serializers.TransactionSerializer(transaction, many=True)
        return Response(serializer.data)


class StudentViewSet(ModelViewSet):
    queryset = Chat.objects.filter(user_type=Chat.ENTRANT).order_by('-id')
    serializer_class = restful_serializers.StudentSerializer
    query_params = filter_params.get_student_params()

    # permission_classes = [IsAdminIsTeacher, ]

    def post(self, request, format=None):
        serializer = UserSerializerWithToken(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_queryset(self):
        queryset = super().get_queryset()
        queryparams = self.request.query_params
        query = queryparams.get('query', None)
        if query:
            queryset = queryset.filter(
                Q(last_name__icontains=query) |
                Q(phone__icontains=query) |
                Q(first_name__icontains=query)
            )
        return queryset

    @swagger_auto_schema(manual_parameters=query_params)
    def list(self, request, *args, **kwargs):
        return super().list(kwargs)


class StudentOrderDownload(generics.RetrieveAPIView):

    def get_object(self, *args, **kwargs):
        pk = self.kwargs.get('pk')
        try:
            return Order.objects.get(pk=pk)
        except Order.DoesNotExist:
            raise Http404

    def get(self, request, *args, **kwargs):
        pk = self.kwargs['pk']
        order = self.get_object(pk)
        with open(os.path.join(settings.MEDIA_ROOT, '{}'.format(order.file)), 'rb') as fh:
            response = HttpResponse(fh.read(), content_type="application/pdf")
            response['Content-Disposition'] = 'attachment; filename={}'.format(order.file.name)
            return response


class TeacherViewSet(ModelViewSet):
    queryset = CustomUser.objects.filter(type_of_user=ExtraData.MENTOR)
    serializer_class = restful_serializers.TeacherSerializer
    # permission_classes = [IsAdminIsTeacher, ]


class GroupViewSet(ModelViewSet):
    queryset = Group.objects.all().order_by('-id')
    serializer_class = restful_serializers.GroupSerializer
    # permission_classes = [IsAdminIsTeacher, ]
    phone = openapi.Parameter('student_phone', openapi.IN_QUERY, description='student phone sini  ni junating',
                              type=openapi.TYPE_STRING, required=True)

    @swagger_auto_schema(manual_parameters=[phone])
    @action(detail=True, methods=['post'])
    def set_student(self, request, pk=None):
        group = self.get_object()
        try:
            phone = request.GET.get('student_phone', '').replace(' ', '+')
            student = Chat.objects.get(phone__exact=phone)
            group.students.add(student)
            group.save()
            serializer = self.serializer_class(group)
            return Response(serializer.data)
        except Exception:
            return Response({'error': 'Student with this id not found'})

    id = openapi.Parameter('student_id', openapi.IN_QUERY, description='student id sini  ni junating',
                           type=openapi.TYPE_STRING, required=True)

    @action(detail=True, methods=['POST'])
    def delete_student(self, request, pk=None, *args, **kwargs):
        student_id = request.data.get('students_id', None)
        group = self.get_object()
        group.students.delete(student_id)
        group.save()
        serializer = self.serializer_class(group)
        return Response(serializer.data)


class ChangePasswordView(generics.UpdateAPIView):
    serializer_class = restful_serializers.ChangePasswordSerializer
    model = CustomUser
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            if not self.object.check_password(serializer.data.get('old_password')):
                return Response({'old_password': ['Wrong password.']}, status=status.HTTP_400_BAD_REQUEST)
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password update successfully',
                'data': []
            }

            return Response(response)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class TokenGenerateView(JWTTokenObtainPairView):
    serializer_class = TokenSerializer
    post_responses = {
        rest_status.HTTP_201_CREATED: openapi.Response(description='Token obtained'),
        rest_status.HTTP_404_NOT_FOUND: openapi.Response(description='User not found'),
        rest_status.HTTP_400_BAD_REQUEST: openapi.Response(description='Validation error'),
    }

    @swagger_auto_schema(operation_id='token_obtain', operation_description='Token obtaining', responses=post_responses)
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        try:
            serializer.is_valid(raise_exception=True)
        except TokenError as e:
            raise InvalidToken(e.args[0])
        else:
            data = serializer.validated_data
        return response.Response(data, status=rest_status.HTTP_201_CREATED)


class TokenRefreshView(JWTTokenRefreshView):
    post_responses = {
        rest_status.HTTP_201_CREATED: openapi.Response(description='Token obtained'),
        rest_status.HTTP_404_NOT_FOUND: openapi.Response(description='Token not found'),
        rest_status.HTTP_400_BAD_REQUEST: openapi.Response(description='Validation error'),
    }

    @swagger_auto_schema(operation_id='token_refreshing', operation_description='Token refreshing',
                         responses=post_responses)
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class ModeratorViewSet(ModelViewSet):
    queryset = CustomUser.objects.filter(type_of_user=ExtraData.MODERATOR).order_by('-id')
    serializer_class = restful_serializers.ModeratorSerializer

    # permission_classes = [permissions.IsAdminUser, ]

    def perform_create(self, serializer):
        instance = serializer.save(type_of_user=ExtraData.MODERATOR)
        instance.set_password(serializer.validated_data.get('password'))
        instance.save()


class WithdrawViewSet(ModelViewSet):
    queryset = Withdraw.objects.all().order_by('-id')
    serializer_class = restful_serializers.WithdrawSerializer

    # permission_classes = [ ]

    def perform_update(self, serializer):
        withdraw = serializer.save()
        moderator = withdraw.user
        moderator.balance -= withdraw.summa
        moderator.save()


class QuestionSettingsViewSet(ModelViewSet):
    queryset = QuestionSetting.objects.all()
    serializer_class = restful_serializers.QuestionSettingsSerializer
    query_params = filter_params.get_test_params()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=isinstance(request.data, list))
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def get_queryset(self):
        queryset = super().get_queryset()
        queryparams = self.request.query_params
        qargs = {}

        number = queryparams.get('number', None)
        if number:
            qargs.update({"number__icontains": number})

        subject = queryparams.get('subject', None)
        if subject:
            qargs.update({'subject__title__icontains': subject})

        test_type = queryparams.get('test_type', None)
        if test_type:
            qargs.update({"test_type__icontains": test_type})
        return queryset.filter(**qargs)

    @swagger_auto_schema(manual_parameters=query_params)
    def list(self, request, *args, **kwargs):
        return super(QuestionSettingsViewSet, self).list(kwargs)

    id = openapi.Parameter('owner', openapi.IN_QUERY, description='send owner username', type=openapi.TYPE_INTEGER)
    id_list = openapi.Parameter('id_list', openapi.IN_QUERY,
                                description='send owner username', type=openapi.TYPE_ARRAY, items=[id], )

    @swagger_auto_schema(manual_parameters=[id_list])
    @action(detail=False, methods=['GET'])
    def delete_list(self, request, *args, **kwargs):
        query_params = self.request.query_params
        id_list = query_params.get('id_list', None)
        if id_list and (isinstance(id_list, list) or isinstance(id_list.split(','), list)):
            querset = QuestionSetting.objects.filter(id__in=id_list) if isinstance(id_list,
                                                                                   list) else QuestionSetting.objects.filter(
                id__in=id_list.split(','))
            querset.delete()
            return Response({"success": "success"}, status=status.HTTP_200_OK)
        else:
            return Response({"Bad Request": "asdfasd"}, status=status.HTTP_400_BAD_REQUEST)


class UserView(generics.UpdateAPIView):
    permission_classes = [IsAuthenticated]
    serializer_class = restful_serializers.UserSerializerUpdate

    def get_object(self):
        return self.request.user


class PaymentHistoryViewSet(APIView):
    def get(self, request, format=None):
        paymentHistory = PaymentHistory.objects.all()
        serializer = restful_serializers.PaymentHistorySerializer(paymentHistory, many=True)
        return Response(serializer.data)
