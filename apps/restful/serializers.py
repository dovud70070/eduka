from django.db.models import Sum
from rest_framework import serializers
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from apps.main.models import *
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from apps.main.models import Withdraw


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = CustomUser
        fields = ['id', 'type_of_user', 'username', 'password', 'first_name', 'last_name']


class UserSerializerUpdate(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['username', 'first_name', 'last_name', 'type_of_user']


class UserSerializerWithToken(serializers.ModelSerializer):
    token = serializers.SerializerMethodField()
    password = serializers.CharField(write_only=True)
    type_of_user = serializers.CharField(read_only=True)
    first_name = serializers.CharField(read_only=True)
    last_name = serializers.CharField(read_only=True)

    class Meta:
        model = CustomUser
        fields = ['token', 'type_of_user', 'username', 'first_name', 'last_name']

    def get_token(self, obj):
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(obj)
        token = jwt_encode_handler(payload)
        return token

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        instance = self.Meta.model(**validated_data)
        if password is not None:
            instance.set_password(password)
        instance.save()
        return instance


class TokenSerializer(TokenObtainPairSerializer):
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        data['refresh'] = str(refresh)
        data['access'] = str(refresh.access_token)
        data['user'] = UserSerializer(instance=self.user).data
        return data


class StudentSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Chat
        fields = ['id', 'user', 'user_type', 'first_name', 'last_name', 'phone']


class TeacherSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=70, write_only=True)

    class Meta:
        model = CustomUser
        fields = ['id', 'type_of_user', 'username', 'first_name', 'last_name', 'phone', 'password']


class PurchaserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        fields = "__all__"


class SubjectSerializerForQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ['id', 'title']


class SectionForTopicSerializer(serializers.ModelSerializer):
    class Meta:
        model = Section
        fields = ['id', 'title']


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = ['id', 'title', 'is_thematic']


class ClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = Class
        fields = "__all__"


class SectionSerializer(serializers.ModelSerializer):
    subject = serializers.PrimaryKeyRelatedField(queryset=Subject.objects.all())

    class Meta:
        model = Section
        fields = "__all__"

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['subject'] = SubjectSerializerForQuestionSerializer(instance.subject).data
        return data


class ClassSerializerForQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Class
        fields = ['id', 'title']


class TopicSerializer(serializers.ModelSerializer):
    section = serializers.PrimaryKeyRelatedField(queryset=Section.objects.all())
    subject = serializers.PrimaryKeyRelatedField(queryset=Subject.objects.all())
    _class = serializers.PrimaryKeyRelatedField(queryset=Class.objects.all())

    class Meta:
        model = Topic
        fields = "__all__"

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['_class'] = ClassSerializerForQuestionSerializer(instance._class).data
        data['section'] = SectionForTopicSerializer(instance.section).data
        data['subject'] = SubjectSerializerForQuestionSerializer(instance.subject).data
        return data


class TopicForQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = ['id', 'title']


class VariantForQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Variant
        fields = ['id', 'title', 'description', 'is_correct']


class OrderSerializerForItem(serializers.ModelSerializer):
    total_price = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = ['id', 'purchaser', 'total_price', 'created_at', 'is_paid', 'test_type']

    def get_total_price(self, obj):
        return 1


class OrderItemSerializerForQuestion(serializers.ModelSerializer):
    order = OrderSerializerForItem(read_only=True)

    class Meta:
        model = OrderItem
        fields = ['id', 'order', 'price', 'is_solved_correctly']


class OwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['id', 'type_of_user', 'username', 'first_name', 'last_name']


class QuestionUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = '__all__'


class SolutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Solution
        fields = ['text_solution', ]


class QuestionSerializer(serializers.ModelSerializer):
    _class = serializers.PrimaryKeyRelatedField(queryset=Class.objects.all())
    topic = serializers.PrimaryKeyRelatedField(queryset=Topic.objects.all())
    subject = serializers.PrimaryKeyRelatedField(queryset=Subject.objects.all())
    variants = VariantForQuestionSerializer(many=True)
    items = OrderItemSerializerForQuestion(many=True, required=False, read_only=True)
    owner = OwnerSerializer(read_only=True)
    total_price = serializers.SerializerMethodField()

    class Meta:
        model = Question
        fields = ['id', 'title', 'topic', 'description', 'is_mandatory', 'is_state_center_test', 'is_themed', '_class',
                  'subject', 'level', 'variants', 'items', 'owner', 'total_price', 'enabled', 'text_solution']

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['topic'] = TopicForQuestionSerializer(instance.topic).data
        data['subject'] = SubjectSerializerForQuestionSerializer(instance.subject).data
        data['_class'] = ClassSerializerForQuestionSerializer(instance._class).data
        data['variants'] = VariantForQuestionSerializer(instance.variants, many=True).data
        data['ordered_items_count'] = instance.ordered_items.count()
        return data

    def get_total_price(self, obj):
        return 0

    def create(self, validated_data):
        variants = validated_data.pop('variants')
        obj = Question.objects.create(**validated_data)
        choices_variants = ["A", "B", "C", "D"]
        var = {'title': 'A'}
        for variant, choices_variant in zip(variants, choices_variants):
            var['title'] = choices_variant
            var.update(variant)
            variant_obj = VariantSerializer(data=var)
            if variant_obj.is_valid():
                variant_obj.save(question=obj)
        return obj

    def update(self, instance, validated_data):
        variants_data = validated_data.pop('variants')
        variants = instance.variants.all()
        variants = list(variants)

        for variant_data in variants_data:
            variant = variants.pop(0)
            variant.description = variant_data.get('description', variant.description)
            variant.is_correct = variant_data.get('is_correct', variant.is_correct)
            variant.save()

        return super().update(instance, validated_data)


class VariantSerializer(serializers.ModelSerializer):
    question = QuestionSerializer(read_only=True)

    class Meta:
        model = Variant
        fields = ['id', 'title', 'is_correct', 'description', 'question']


class OrderItemSerializer(serializers.ModelSerializer):
    question = QuestionSerializer(read_only=True)

    class Meta:
        model = OrderItem
        fields = ['id', 'question', 'is_solved_correctly']


class OrderSerializer(serializers.ModelSerializer):
    purchaser = PurchaserSerializer(read_only=True)
    items = OrderItemSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = ['id', 'purchaser', 'items', 'total_price', 'created_at', 'file', 'is_paid', 'test_type']


class ChatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Chat
        fields = ['id', 'first_name', 'last_name', 'phone']


class TransactionSerializer(serializers.ModelSerializer):
    owner = ChatSerializer(read_only=True)

    class Meta:
        model = Transaction
        fields = "__all__"


class StudentSerializerForGroup(serializers.ModelSerializer):
    class Meta:
        model = Chat
        fields = ['id', 'first_name', 'last_name']


class MentorSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['id', 'type_of_user', 'username']


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['id', 'title', 'students']

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['students_count'] = instance.students.count()
        data['students_list'] = ChatSerializer(instance.students, many=True).data
        data['mentors'] = MentorSerializer(instance.chats, many=True).data
        return data

    # def validate(self, attrs):
    #     if attrs['title']

class PasswordSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['password']


class CustomUserSerializerForWithdraw(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['id', 'type_of_user', 'username', 'first_name', 'last_name']


class ModeratorSerializerForWithdraw(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['id', 'type_of_user', 'username']


class WithdrawSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(queryset=CustomUser.objects.all())

    class Meta:
        model = Withdraw
        fields = ['id', 'status', 'summa', 'user']


class WithdrawSerializerForModerator(serializers.ModelSerializer):
    class Meta:
        model = Withdraw
        fields = ['id', 'status', 'summa']


class ModeratorSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=70, write_only=True)
    withdraw = WithdrawSerializerForModerator(read_only=True, many=True)

    class Meta:
        model = CustomUser
        fields = ['id', 'type_of_user', 'username', 'first_name', 'last_name', 'password', 'balance', 'withdraw']


class ChangePasswordSerializer(serializers.Serializer):
    model = CustomUser
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)


class TopicSerializerForQuestionSerializer(serializers.ModelSerializer):
    title = serializers.CharField(read_only=True)

    class Meta:
        model = Topic
        fields = ['id', 'title']


class QuestionLevelTopicSerializer(serializers.ModelSerializer):
    topic = serializers.PrimaryKeyRelatedField(many=True, queryset=Topic.objects.all())

    class Meta:
        model = QuestionLevel
        fields = ['id', 'level', 'topic', 'setting']

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['topic'] = TopicForQuestionSerializer(many=True, instance=instance.topic.all()).data
        return data


class QuestionLevelSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuestionLevel
        fields = ['level', 'setting']


class QuestionSettingsSerializer(serializers.ModelSerializer):
    question_levels = QuestionLevelSerializer(many=True)
    subject = serializers.PrimaryKeyRelatedField(queryset=Subject.objects.all())
    test_type = serializers.CharField(read_only=True)

    class Meta:
        model = QuestionSetting
        fields = ['id', 'subject', 'number', 'question_levels', 'test_type']

    def to_internal_value(self, data):
        levels = data.get('question_levels')
        ret = []
        for item in levels:
            ret.append({"level": item})
        data['question_levels'] = ret
        subject = data.get('subject')
        data['subject'] = Subject.objects.get(id=subject)
        return data

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['subject'] = SubjectSerializerForQuestionSerializer(instance.subject).data
        return data

    def create(self, validated_data):
        levels = validated_data.pop('question_levels')
        obj = QuestionSetting(**validated_data)
        obj.save()
        for level in levels:
            level_obj = QuestionLevelSerializer(data=level)
            if level_obj.is_valid():
                level_obj.save(setting=obj)
        return obj

    def update(self, instance, validated_data):
        levels_data = validated_data.pop('question_levels')
        levels = []
        for level_data in levels_data:
            level, _ = QuestionLevel.objects.get_or_create(level=level_data.get('level'), setting=instance)
            levels.append(level)
        instance.question_levels.clear()
        for level in levels:
            instance.question_levels.add(level)
        return super().update(instance, validated_data)


class IdListSerializer(serializers.Serializer):
    id = serializers.IntegerField()


class PaymentHistorySerializer(serializers.ModelSerializer):
    chat = serializers.CharField(max_length=200)
    user = serializers.CharField(max_length=200)

    class Meta:
        model = PaymentHistory
        fields = ['id', 'amount', 'chat', 'user', 'order', 'created_at']
