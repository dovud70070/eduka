from drf_yasg import openapi


def get_topics_params() -> list:
    subject_id = openapi.Parameter('subject_id', openapi.IN_QUERY, description='send subject id',
                                   type=openapi.TYPE_INTEGER)
    _class_id = openapi.Parameter('_class_id', openapi.IN_QUERY, description='send class id', type=openapi.TYPE_INTEGER)

    return [subject_id, _class_id]


def get_question_params() -> list:
    topic = openapi.Parameter('topic', openapi.IN_QUERY, description='send topic ', type=openapi.TYPE_STRING)
    is_mandatory = openapi.Parameter('is_mandatory', openapi.IN_QUERY, description='is_mandatory',
                                     type=openapi.TYPE_BOOLEAN)
    is_state_center_test = openapi.Parameter('is_state_center_test', openapi.IN_QUERY,
                                             description='is_state_center_test', type=openapi.TYPE_BOOLEAN)
    _class_id = openapi.Parameter('_class_id', openapi.IN_QUERY, description='_class_id', type=openapi.TYPE_INTEGER)
    _class_id_list = openapi.Parameter('_class_id_list', openapi.IN_QUERY,
                                       description='class id ni jonat', type=openapi.TYPE_ARRAY, items=[_class_id])
    subject = openapi.Parameter('subject', openapi.IN_QUERY, description='subject_id', type=openapi.TYPE_INTEGER)
    query = openapi.Parameter('query', openapi.IN_QUERY, description='Search Query', type=openapi.TYPE_STRING)
    title = openapi.Parameter('title', openapi.IN_QUERY, description='send question title', type=openapi.TYPE_STRING)
    form_level = openapi.Parameter('from_level', openapi.IN_QUERY, description='send level', type=openapi.TYPE_STRING)
    to_level = openapi.Parameter('to_level', openapi.IN_QUERY, description='send level', type=openapi.TYPE_STRING)
    subject_id = openapi.Parameter('subject_id', openapi.IN_QUERY, description='subject id ni jonat',
                                   type=openapi.TYPE_INTEGER)
    subject_id_list = openapi.Parameter('subject_id_list', openapi.IN_QUERY,
                                        description='subject id no jonat', type=openapi.TYPE_ARRAY,
                                        items=[subject_id])
    owner = openapi.Parameter('owner', openapi.IN_QUERY, description='send owner username', type=openapi.TYPE_STRING)
    owner_list = openapi.Parameter('owner_list', openapi.IN_QUERY,
                                   description='send owner username', type=openapi.TYPE_ARRAY, items=[owner])
    enabled = openapi.Parameter('enabled', openapi.IN_QUERY, description='enabled', type=openapi.TYPE_BOOLEAN)

    return [is_mandatory, is_state_center_test, topic, title, subject_id,
            subject_id_list, _class_id, _class_id_list, form_level, to_level, owner_list, enabled, ]


def get_student_params() -> list:
    phone = openapi.Parameter('phone', openapi.IN_QUERY, description='send phone', type=openapi.TYPE_STRING)
    first_name = openapi.Parameter('first_name', openapi.IN_QUERY, description='send student first_name',
                                   type=openapi.TYPE_STRING)
    last_name = openapi.Parameter('last_name', openapi.IN_QUERY, description='send student last_name',
                                  type=openapi.TYPE_STRING)
    query = openapi.Parameter('query', openapi.IN_QUERY, description='Search Query', type=openapi.TYPE_STRING)
    return [query]


def get_test_params() -> list:
    number = openapi.Parameter('number', openapi.IN_QUERY, description='number', type=openapi.TYPE_INTEGER)
    subject = openapi.Parameter('subject', openapi.IN_QUERY, description='subject', type=openapi.TYPE_STRING)
    test_type = openapi.Parameter('test_type', openapi.IN_QUERY, description='test type', type=openapi.TYPE_STRING)
    return [number, subject, test_type]

