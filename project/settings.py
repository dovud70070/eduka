import os
from datetime import timedelta

import environ
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent
env = environ.Env(
    # set casting, default value
    DEBUG=(bool, False)
)
# reading .env file
environ.Env.read_env()

# False if not in os.environ
DEBUG = env('DEBUG')

# Raises django's ImproperlyConfigured exception if SECRET_KEY not in os.environ
SECRET_KEY = env('SECRET_KEY')

ALLOWED_HOSTS = ["*", ]

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'apps.payment',
    'apps.bot',
    'apps.main',
    'apps.restful',
    'ckeditor',
    'ckeditor_uploader',
    'rest_framework',
    'rest_framework_swagger',
    'corsheaders',
    'drf_yasg',
    'xhtml2pdf',
    'clickuz',
    'debug_toolbar',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

INTERNAL_IPS = [
    'localhost',
    '127.0.0.1',
]

ROOT_URLCONF = 'project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'libraries': {  # Adding this section should work around the issue.
                'staticfiles': 'django.templatetags.static',
            },
        },
    },
]

WSGI_APPLICATION = 'project.wsgi.application'
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_REGEX_WHITELIST = [
    'http://192.168.0.129:8080',
]
DATABASES = {
    'default': env.db(),
    'extra': env.db('SQLITE_URL', default='sqlite:////tmp/my-tmp-sqlite.db')
}

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
REST_FRAMEWORK = {
    'DEFAULT_SCHEMA_CLASS': 'rest_framework.schemas.coreapi.AutoSchema',
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_simplejwt.authentication.JWTAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 12
}

JWT_AUTH = {
    'JWT_RESPONSE_PAYLOAD_HANDLER': 'apps.account_account.utils.my_jwt_response_handler',
    # 'ACCESS_TOKEN_LIFETIME': timedelta(days=3),
    # 'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
    # 'SLIDING_TOKEN_LIFETIME': timedelta(days=3),
    # 'SLIDING_TOKEN_REFRESH_LIFETIME': timedelta(days=1),
}

SIMPLE_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(days=365),
    'REFRESH_TOKEN_LIFETIME': timedelta(days=1),
}

# AUTHENTICATION_BACKENDS = (
#     'django.contrib.auth.backends.ModelBackend',
#     'allauth.account.auth_backends.AuthenticationBackend',
# )

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
# TG_TOKEN = env('TG_TOKEN')
WEBHOOK_URL = env('WEBHOOK_URL')
STATIC_ROOT = os.path.join(BASE_DIR, 'assets')
STATICFILES_DIRS = [os.path.join(BASE_DIR, "static")]
MEDIA_ROOT = BASE_DIR / 'media'
MEDIA_URL = '/media/'
ITEM_PRICE = 2000
TOTAL_PRICE = 2000
TG_PAGINATE_BY = 5
CKEDITOR_UPLOAD_PATH = 'ckeditor'
AUTH_USER_MODEL = 'main.CustomUser'

PAYCOM_SETTINGS = {
    "PAYCOM_ENV": False,  # test host
    "TOKEN": "token",  # token
    "SECRET_KEY": "password",  # password
    "ACCOUNTS": {
        "KEY_1": "order_id",
        "KEY_2": None  # or "type"
    }
}

CKEDITOR_CONFIGS = {
    'default': {
        'skin': 'moono',
        'toolbar_Custom': [
            {'name': 'math', 'items': ['Mathjax', 'Source']},
        ],
        'toolbar': 'Custom',
        'mathJaxLib': '//cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML',
        'extraPlugins': ','.join(['mathjax', ]),
    },
}

CLICK_SETTINGS = {
    'service_id': '1',
    'merchant_id': '1',
    'secret_key': '1'
}
